<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>약품 입력</title>
<script src="<c:url value="/js/jquery-3.1.1.min.js"/>"></script>
</head>

<script type="text/javascript">
$(document).ready(function(){
	
	
	$("#logout").click(function(){
		
		window.location.href="<c:url value="/user/logout"/>";
		
	});

	
	$("#next").click(function(){
		var medicineList = [];
		var number = $("#medicineTable tr").length
		medicineList.push($("#medicineTable tr td:nth-child(1)").text() + " ");
		var result = medicineList.toString();
		$("#medicineList").val(result);
		
	});
	
	$("#delete").click(function(){
		$("#medicineTable tr ").has('input[name="checkMedicine"]:checked').remove();
		return false;
	});
	
	$("#input").click(function(){
	
		if($("#medicineCode").val() == ""){
			alert("약품 코드를 입력하세요!");
			$("#medicineCode").focus();
			return false;
		}
		
		var medicineCode= $("#medicineCode").val();
		
		if($("#medicineTable").text().indexOf(medicineCode) != -1){
			alert("이미 목록에 추가된 약품 코드입니다.");
			$("#medicineCode").val("");
			return false;
		}
		
	
		var request = $.ajax({
			type: "POST",
			url: "<c:url value="/prescription/medicine_check"/>",
			data: {medicineCode : medicineCode
					},
			dataType: "json",
			success: function(jsonObj){
				
				if(jsonObj.msg != null)
				{
					alert(jsonObj.msg);	
				}
				else if(jsonObj.isExist == false){
					alert("입력하신 약품 코드는 존재하지 않습니다.");
				}
				else{
					var time = new Date().toLocaleTimeString();
					$("#medicineTable > tbody:last").append("<tr><td>"+jsonObj.medicineCode+" "+"</td><td>"+jsonObj.medicineName+"</td><td>"+
							jsonObj.atcCode+"</td><td>"+jsonObj.atcName+"</td><td>"+'<input type="checkbox" name="checkMedicine"/>' + "</td></tr>");
					$("#medicineCode").val("");

					
				}
				
			}
			
		});
				event.preventDefault();
	});
	
});

		
		
</script>
	

<body>
<form id="inputMedicineForm" action="<c:url value="/prescription/confirmProhibition"/>" method="post">
		약품 코드:<Input type="text" name="medicineCode" id="medicineCode"/>
		<Button name="input" id="input">입력하기</Button>
		<Button name="delete" id="delete">삭제하기</Button>
		<br>
		<br>
		<table id="medicineTable" border="1" cellspacing="0">
			<tr>

				<th>약품코드</th>
				<th>약품이름</th>
				<th>ATC코드</th>
				<th>ATC성분</th>
				<th>선택(삭제할 목록)</th>

			</tr>
			<tbody></tbody>
		</table>
		<br>
		
		<Input type="hidden" id="doctorId" size=10 name="doctorId" value="${doctorId}">
		<Input type="hidden" id="medicineList" name="medicineList">
		<Button name="next" id="next">알레르기확인</Button>
</form>
<br><br>

<Button name="logout" id="logout">로그아웃</Button>

</body>
</html>