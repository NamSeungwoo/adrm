<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>나의 의사 목록</title>
<script src="<c:url value="/js/jquery-3.1.1.min.js"/>"></script>
</head>
<script type="text/javascript">
	<c:set value="${user }" var="user"/>
	<c:if test="${user == null}">
	$(document).ready(function() {
		alert("로그인이 필요한 페이지입니다.");
		window.location.href = "<c:url value="/index.jsp"/>";
	});
	</c:if>
	
	
	$(document)
			.ready(
					function() {
						$("#logout")
								.click(
										function() {

											window.location.href = "<c:url value="/user/logout"/>";

								});

					});
</script>
<body>
	<table id="patientTable" border=1>
		<TR>
			<TD width=40>순번</TD>
			<TD width=120>의사ID</TD>
			<TD width=120>의사이름</TD>
			<TD width=100>의사성별</TD>

		</TR>
				
		<c:set var="cnt" value="0" />
		<c:forEach items="${myDoctorList}" var="list">
			<c:set var="cnt" value="${cnt+1 }" />
			<TR>
				<TD><c:out value="${cnt }" /></TD>
				<TD><a
					href="<c:url value="/prescription/doctorSelect?doctorId=${list.doctor.userId}"/>"><c:out
							value="${list.doctor.userId}" /></a></TD>
				<TD><c:out value="${list.doctor.name}" /></TD>
				<TD><c:out value="${list.doctor.gender}" /></TD>

			</TR>
		</c:forEach>
	
	</table>
	
	<br><br>
	<Button name="logout" id="logout">로그아웃</Button>
</body>
</html>