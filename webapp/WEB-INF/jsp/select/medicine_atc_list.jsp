<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="EUC-KR"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<script src="<c:url value="/js/jquery-3.1.1.min.js"/>"></script>
<title>약품으로 찾은 ATC list</title>
</head>
<script>
	$(document).ready(function(){
		$("#back").click(function(){
			window.location.href = "<c:url value="/medicine/search"/>";
		});
	});
</script>
<body>
	<H4>검색 결과</H4>
	<table border=1>
		<TR>
			<TD width=40>순번</TD>
			<TD width=40>대표코드</TD>
			<TD width=500>제품명(Full name)</TD>
			<TD width=40>ATC코드</TD>
			<TD width=40>ATC성분</TD>
		</TR>
		<c:set var="cnt" value="0"/>
		<c:forEach var="medicine" items="${MEDICINE_ATC_LIST}">
		<c:set var="cnt" value="${cnt+1 }"/>
		<TR>
			<TD><c:out value="${cnt }"/></TD>
			<TD><c:out value="${medicine.code }"/></TD>
			<TD><c:out value="${medicine.fullName }"/></TD>
			<TD><c:out value="${medicine.atc.atcCode }"/></TD>
			<TD><c:out value="${medicine.atc.atcName }"/></TD>
		</TR>
		</c:forEach>
	</table>
	<Button id="back">이전으로</Button>
</body>
</html>