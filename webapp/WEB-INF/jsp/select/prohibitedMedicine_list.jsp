<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="EUC-KR"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>

<title>prohibited Medicine list</title>
<script src="<c:url value="/js/jquery-3.1.1.min.js"/>"></script>
</head>
<script type="text/javascript">
	<c:set value="${user }" var="user"/>
	<c:if test="${user == null}">
		$(document).ready(function(){
			alert("로그인이 필요한 페이지입니다.");
			window.location.href="<c:url value="/index.jsp"/>";
		});
	</c:if>
	
	$(document).ready(function(){
		$("#logout").click(function(){
			
			window.location.href="<c:url value="/user/logout"/>";
			
		});
		$("#ok").click(function(){
			
			window.location.href="<c:url value="/prescription/insertAlergyFinal"/>";
		});
		$("#back").click(function(){
			
			parent.history.back();
			return false;
		});
	});
	
</script>
<body>
	<H4>금지 약제 검색 결과</H4>
	<c:out value="아래는 금지약제:${prohibition}에 해당하는 회피약제 리스트 입니다. 이 약제를 금지약제로 입력하시겠습니까?"/>
	
	<Button name="ok" id="ok">금지약제 최종입력</Button>
	<Button name="back" id="back">뒤로가기</Button>
	<table border=1>
		<TR>
			<TD width=40>순번</TD>
			<TD width=40>약제코드</TD>
			<TD width=500>약제이름</TD>
			<TD width=40>ATC코드</TD>
			<TD width=40>ATC이름</TD>
		</TR>
		<c:set var="cnt" value="0"/>
		<c:forEach var="medicine" items="${BAN_LIST}">
		<c:set var="cnt" value="${cnt+1 }"/>
		<TR>
			<TD><c:out value="${cnt }"/></TD>
			
			<TD><c:out value="${medicine.code }"/></TD>
			<TD><c:out value="${medicine.fullName }"/></TD>
			<TD><c:out value="${medicine.atc.atcCode }"/></TD>
			<TD><c:out value="${medicine.atc.atcName }"/></TD>
		</TR>
		</c:forEach>
	</table>
</body>
</html>