<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="EUC-KR"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>

<title>승인 list</title>
</head>
<script src="<c:url value="/js/jquery-3.1.1.min.js"/>"></script>
<script type="text/javascript">
	var regExp = /\s/g;
	<c:set value="${user }" var="user"/>
	<c:if test="${user == null}">
		$(document).ready(function(){
			alert("로그인이 필요한 페이지입니다.");
			window.location.href="<c:url value="/index.jsp"/>";
		});
	</c:if>
	
	$(document).ready(function(){
		$("#logout").click(function(){
			
			window.location.href="<c:url value="/user/logout"/>";
			
		});
		
		$("#changeButton").click(function(){
			alert("변경사항이 저장되었습니다.");
			$("#changeInform").submit();
		});
		
			$("#searchButton").click(function(){
				if($("#search").val() =="" || regExp.test($("#search").val())){
					alert("검색어를 바르게 입력하세요(공백X).");
					return false;
				}
				$("#searchForm").submit();
			});
		
	});
	
	
</script>
<body>

	<form id="changeInform" action="<c:url value="/user/approvalManagementFinal"/>" method="post">	
	<H4>유저 리스트</H4>
	<table border=1>
		<TR>
			<TD width=40>순번</TD>
			<TD width=150>아이디</TD>
			<TD width=150>승인요청날짜</TD>
			<TD width=100>승인 및 거부</TD>
		</TR>
		<c:set var="start" value="${paging.pg*paging.pageSize}"/>
		<c:set var="cnt" value="${paging.pg*paging.pageSize}"/>
		
		<c:forEach var="approval" items="${approvalList}">
			<c:set var="cnt" value="${cnt+1 }"/>
		<TR>
			<TD><c:out value="${cnt }"/></TD>
			<TD><c:out value="${approval.userId }"/></TD> 
			<TD><fmt:formatDate value="${approval.date }"  pattern="yyyy-MM-dd hh:mm"/></TD>
			<TD>
			<c:choose>
				
				<c:when test="${approval.approval.value == 3 }">
					승인<input id='disable<c:out value="${cnt }"/>' type="radio" name='disable<c:out value="${cnt }"/>' value="APPROVAL">
					<br>거부<input id='disable<c:out value="${cnt }"/>' type="radio" name='disable<c:out value="${cnt }"/>' value="REJECTION">
				</c:when>
				<c:when test="${approval.approval.value == 1 }">
					승인<input id='disable<c:out value="${cnt }"/>' type="radio" name='disable<c:out value="${cnt }"/>' value="APPROVAL" checked>
					<br>거부<input id='disable<c:out value="${cnt }"/>' type="radio" name='disable<c:out value="${cnt }"/>' value="REJECTION">
				</c:when>
				<c:when test="${approval.approval.value == 2 }">
					승인<input id='disable<c:out value="${cnt }"/>' type="radio" name='disable<c:out value="${cnt }"/>' value="APPROVAL">
					<br>거부<input id='disable<c:out value="${cnt }"/>' type="radio" name='disable<c:out value="${cnt }"/>' value="REJECTION" checked>
				</c:when>
				
			</c:choose>
			</TD>
			
		</TR>
		</c:forEach>
	</table>
	
		<a href="<c:url value="/user/approvalManagement?pg=0"/>">[목록]</a>
	
		<c:set var="i" value="${start}"/>
		<c:forEach var="approval" items="${approvalList}">
			<c:set var="i" value="${i+1 }"/>
			<input type="hidden" name='userId<c:out value="${i}"/>' value="${approval.userId }">
			
		</c:forEach>
		<input type="hidden" name="start" value="${start }">
		<input type="hidden" name="cnt" value="${i}">
	</form>
	<Button id="changeButton" name="changeButton">변경사항 적용하기</Button>
	<br><br>
	<form id="searchForm" action="<c:url value="/user/approvalManagementSearch"/>" method="post">
		검색<input type="text" id="search" name="search">
		<select name="searchKind">
		<option value="id" selected>아이디로 검색</option>
		</select>
	</form>
	<Button id="searchButton" name="searchButton">검색하기</Button>
	<Button name="logout" id="logout">로그아웃</Button>	
	
</body>
</html>