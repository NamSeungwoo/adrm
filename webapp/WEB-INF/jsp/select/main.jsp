<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script src="<c:url value="/js/jquery-3.1.1.min.js"/>"></script>
<title>메인페이지</title>

</head>
<script type="text/javascript">
	
	<c:set value="${user }" var="user"/>
	<c:if test="${user == null}">
		$(document).ready(function(){
			alert("로그인이 필요한 페이지입니다.");
			window.location.href="<c:url value="/index.jsp"/>";
		});
	</c:if>
	
	$(document).ready(function(){
		$("#logout").click(function(){
			window.location.href="<c:url value="/user/logout"/>";
		});
	});
</script>
<body>
	
	
	<br><br>

	<c:forEach items="${user.roles }" var="role">
		
		<c:if test="${role.userRole.value == 1 }">
			<LI><a href="<c:url value="/user/userManagement"/>">사용자관리</a></LI>
			<LI><a href="<c:url value="/medicine/medicineManagement"/>">약품관리</a></LI>
		</c:if>
		
		<c:choose>
			<c:when test="${(role.userRole.value == 2) && (notice != null)}">
				<c:out value="의사 승인이 거절되었습니다. 회원정보수정으로 올바른 역할을 선택해주세요."/>
			</c:when>
			<c:when test="${fn:length(user.roles) eq 1 && role.userRole.value == 2 }"> 
				<LI><a href="<c:url value="/prescription/doctorDetails"/>">환자리스트</a></LI>
			</c:when>
			<c:when test="${fn:length(user.roles) eq 1 && role.userRole.value == 3 }"> 
				<LI><a href="<c:url value="/prescription/patientDetail"/>">알레르기정보확인</a></LI>
			</c:when>
			<c:when test="${role.userRole.value == 2 }"> 
				<LI><a href="<c:url value="/prescription/doctorDetails"/>">환자리스트</a></LI>
			</c:when>
			<c:when test="${role.userRole.value == 3 }">
				<LI><a href="<c:url value="/prescription/patientDetail"/>">알레르기정보확인</a></LI>
			</c:when>
		</c:choose>
	</c:forEach>
	<LI><a href="<c:url value="/user/modifyMember"/>">회원정보수정</a></LI>
	<Button name="logout" id="logout">로그아웃</Button>

</body>
</html>