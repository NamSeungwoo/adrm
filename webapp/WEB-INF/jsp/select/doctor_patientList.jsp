<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>나의 환자 목록</title>
<script src="<c:url value="/js/jquery-3.1.1.min.js"/>"></script>
</head>
<script type="text/javascript">
	<c:set value="${user }" var="user"/>
	<c:if test="${user == null}">
	$(document).ready(function() {
		alert("로그인이 필요한 페이지입니다.");
		window.location.href = "<c:url value="/index.jsp"/>";
	});
	</c:if>
	
	
	$(document)
			.ready(
					function() {
						$("#logout")
								.click(
										function() {

											window.location.href = "<c:url value="/user/logout"/>";

								});
						$("#addPatient")
								.click(
										function() { 
								});
						$("#deletePatient").click(function(){
							if(confirm("환자를 삭제하시면 처방 기록도 삭제됩니다. 정말로 삭제하시겠습니까?"))
								$("#deletePatientNext").submit();
							
						});
					});
</script>
<body>
	<table id="patientTable" border=1>
		<TR>
			<TD width=40>순번</TD>
			<TD width=120>환자ID</TD>
			<TD width=120>환자충북대코드</TD>
			<TD width=100>환자이름</TD>
			<TD width=100>환자성별</TD>
			<TD width=100>선택(삭제)</TD>
		</TR>
		
		<form id="deletePatientNext" action="<c:url value="/prescription/deletePatient"/>" method="post">		
		<c:set var="cnt" value="0" />
		<c:forEach items="${patientList}" var="list">
			<c:set var="cnt" value="${cnt+1 }" />
			<TR>
				<TD><c:out value="${cnt }" /></TD>
				<TD><a
					href="<c:url value="/prescription/patientSelect?patientId=${list.patient.userId}"/>"><c:out
							value="${list.patient.userId}" /></a></TD>
				<TD><c:out value="${list.patient.cbnuCode}" /></TD>
				<TD><c:out value="${list.patient.name}" /></TD>
				<TD><c:out value="${list.patient.gender}" /></TD>
				<TD>
					<input type="checkbox" id="checkPatient" name="checkPatient" value="<c:out value="${list.patient.userId}"/>">
				
				</TD>

			</TR>
		</c:forEach>
		</form>
	
	</table>
	
	
	<form id="addPatientNext" action="<c:url value="/prescription/searchPatient"/>" method="post">
		<div id="patientList">
		<c:set var="cnt" value="1"/>
		<c:forEach items="${patientList }" var="unit">
			<Input type="hidden" id="unit<c:out value="${cnt }"/>" name="unit<c:out value="${cnt }"/>" value="<c:out value="${unit.patient.userId }"/>">
			<c:set var="cnt" value="${cnt+1 }"/>
		</c:forEach>
		</div>
	<Button name="addPatient" id="addPatient">환자추가</Button><br>
	</form>
	<br>
	<Button name="deletePatient" id="deletePatient">환자삭제</Button><br>	
	<br><br>
	<Button name="logout" id="logout">로그아웃</Button>
</body>
</html>