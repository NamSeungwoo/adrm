<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>복용약제 입력 페이지</title>
<script src="<c:url value="/js/jquery-3.1.1.min.js"/>"></script>
</head>

<script type="text/javascript">
$(document).ready(function(){
	var regExp = /\s/g;
	var upperList = "";
	var prohibitionList = "";
	$("#upperList").children().each(function() {
		upperList += $(this).val()+" ";
	});
	
	$("#prohibitionList").children().each(function() {
		prohibitionList += $(this).val()+" ";
	});
	
	$("#logout").click(function(){
		
		window.location.href="<c:url value="/user/logout"/>";
		
	});

	$("#next").click(function(){
		var medicineList = [];
		var number = $("#medicineTable tr").length
		medicineList.push($("#medicineTable tr td:nth-child(1)").text() + " ");
		var result = medicineList.toString();
		$("#medicineList").val(result);
	});
	
	$("#delete").click(function(){
		$("#medicineTable tr ").has('input[name="checkMedicine"]:checked').remove();
		return false;

	});
	
	$("#input").click(function(){
		
		if($("#capable").val() == "" || regExp.test($("#capable").val())){
			alert("약품코드를 입력하세요!(공백X)");
			$("#capable").focus();
			return false;
		}
		
		var capable= $("#capable").val();
		
		if($("#medicineTable").text().indexOf(capable) != -1){
			alert("이미 목록에 추가된 약품입니다.");
			$("#capable").val("");
			return false;
		}
		
	
		var request = $.ajax({
			type: "POST",
			url: "<c:url value="/prescription/capable_check"/>",
			data: {capable:capable,
					upperList:upperList,
					prohibitionList:prohibitionList},
			dataType: "json",
			success: function(jsonObj){
				if(jsonObj.msg != null)
				{
					alert(jsonObj.msg);	
					$("#capable").val("");
				}
				else if(jsonObj.isExist == false){
					alert("입력하신 약품은 존재하지 않습니다.");
					$("#capable").val("");
				}
				else{
					var time = new Date().toLocaleTimeString();
					$("#medicineTable > tbody:last").append("<tr><td>"+jsonObj.medicineCode+"</td><td>"+jsonObj.medicinefullName+"</td><td>"
							+jsonObj.medicineATCcode+"</td><td>"+jsonObj.medicineATCname+"</td><td>"+'<input type="checkbox" name="checkMedicine"/>' + "</td></tr>");
					$("#capable").val("");
					
				}
				
			}
			
		});
				event.preventDefault();
	});
	
});

		
		
</script>
	

<body>
<form id="loginform" action="<c:url value="/prescription/insertAlergyThird"/>" method="post">
		복용가능약제:<Input type="text" name="capable" id="capable"/>
		<Button name="input" id="input">입력하기</Button>
		<Button name="delete" id="delete">삭제하기</Button>
		<br>
		<br>
		<table id="medicineTable" border="1" cellspacing="0">
			<tr>
				<th>약제코드</th>
				<th>약제이름</th>
				<th>ATC코드</th>
				<th>ATC성분</th>
				<th>선택(삭제할 목록)</th>
			</tr>
			<tbody></tbody>
		</table>
		<br>
		
		<div id="prohibitionList">
		<c:set var="cnt" value="1"/>
		<c:forEach items="${prohibitionList }" var="prohibition">
			<Input type="hidden" id="prohibition<c:out value="${cnt }"/>" name="prohibition<c:out value="${cnt }"/>" value="<c:out value="${prohibition.prohibitedMedicine.code }"/>">
			<c:set var="cnt" value="${cnt+1 }"/>
		</c:forEach>
		</div>
		
		<div id="upperList">
		<c:set var="cnt" value="1"/>
		<c:forEach items="${upperList }" var="upper">
			<Input type="hidden" id="upper<c:out value="${cnt }"/>" name="upper<c:out value="${cnt }"/>" value="<c:out value="${upper.atc.atcCode }"/>">
			<c:set var="cnt" value="${cnt+1 }"/>
		</c:forEach>
		</div>
		
		<Input type="hidden" id="medicineList" size=10 name="medicineList">
		<Button name="next" id="next">다음단계</Button>
</form>
<br><br>

<Button name="logout" id="logout">로그아웃</Button>

</body>
</html>