<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script src="<c:url value="/js/jquery-3.1.1.min.js"/>"></script>
<title>환자 검색 결과</title>
</head>
<script type="text/javascript">
$(document).ready(function(){
	$("#logout").click(function(){
		
		window.location.href="<c:url value="/user/logout"/>";
		
	});
	$("#addPatient").click(function(){
		if($("new").val() == $("existing").val())
			alert("이미 있는 환자입니다.");
		false;
	})

});
</script>
<body>
<table border=1>
	<TR>
		<TD width=120>환자충북대코드</TD>
		<TD width=80>환자ID</TD>
		<TD width=90>환자이름</TD>
		<TD width=90>환자성별</TD>
	</TR>
	<c:set value="${patient}" var="result"></c:set>
	<c:set value="${compare}" var="existing"></c:set>
	<c:set value="${result.cbnuCode }" var="new"></c:set>
	<TR>
		<TD><c:out value="${result.cbnuCode}"/></TD>
		<TD><c:out value="${result.userId}"/></TD>
		<TD><c:out value="${result.name}"/></TD>
		<TD><c:out value="${result.gender}"/></TD>
		
	</TR>
	
</table>
<Button name="logout" id="logout">로그아웃</Button>
<Button name="addPatient" id="addPatient">환자추가</Button>
</body>
</html>