<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>맞는 처방?</title>
<script src="<c:url value="/js/jquery-3.1.1.min.js"/>"></script>
</head>
<script type="text/javascript">
	<c:set value="${user }" var="user"/>
	<c:if test="${user == null}">
	$(document).ready(function() {
		alert("로그인이 필요한 페이지입니다.");
		window.location.href = "<c:url value="/index.jsp"/>";
	});
	</c:if>
	
	
	$(document)
			.ready(
					function() {
						$("#logout")
								.click(
										function() {

											window.location.href = "<c:url value="/user/logout"/>";

								});
					});
</script>
<body>

		<c:if test="${prohibitionList != null}">
		<H4><font color="red">복용을 금지해야할 약물이 목록에 있습니다!</font></H4>
			<table id="prohibitionTable" border=1>
			<TR>
			<TD width=40>순번</TD>
			<TD width=120>금지약제코드</TD>
			<TD width=120>금지약제이름</TD>

			</TR>
				
			<c:set var="cnt" value="0" />
			<c:forEach items="${prohibitionList}" var="prohibition">
			<c:set var="cnt" value="${cnt+1 }" />
			<TR>
				<TD><c:out value="${cnt }" /></TD>
				<TD><strong><c:out value="${prohibition.code}" /></strong> </TD>
				<TD><c:out value="${prohibition.fullName}" /></TD>

			</TR>
			</c:forEach>
			</table>
		</c:if>
		

		<c:if test="${avoidanceList != null}">
		<H4><font color="red">복용을 회피해야할 약물이 목록에 있습니다!</font></H4>
		<table id="avoidanceTable" border=1>
		<TR>
			<TD width=40>순번</TD>
			<TD width=120>회피약제코드</TD>
			<TD width=120>회피약제이름</TD>

		</TR>		
		<c:set var="cnt" value="0" />
		<c:forEach items="${avoidanceList}" var="avoidance">
			<c:set var="cnt" value="${cnt+1 }" />
			<TR>
				<TD><c:out value="${cnt }" /></TD>
				<TD><strong> <c:out value="${avoidance.code}" /></strong> </TD>
				<TD><c:out value="${avoidance.fullName}" /></TD>

			</TR>
		</c:forEach>
		</table>
		</c:if>
	<br><br>
	<Button name="logout" id="logout">로그아웃</Button>
</body>
</html>