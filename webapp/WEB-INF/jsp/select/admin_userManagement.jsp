<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script src="<c:url value="/js/jquery-3.1.1.min.js"/>"></script>
<title>메인페이지</title>

</head>
<script type="text/javascript">
	<c:set value="${user }" var="user"/>
	<c:if test="${user == null}">
		$(document).ready(function(){
			alert("로그인이 필요한 페이지입니다.");
			window.location.href="<c:url value="/index.jsp"/>";
		});
	</c:if>
	
	$(document).ready(function(){
		$("#logout").click(function(){
			
			window.location.href="<c:url value="/user/logout"/>";
			
		});
	});
</script>

<body>
		<LI><a href="<c:url value="/user/accountManagement"/>">계정 삭제 및 활성화</a></LI><br>
		<LI><a href="<c:url value="/user/approvalManagement"/>">의사 요청 승인</a></LI>
		
		<Button name="logout" id="logout">로그아웃</Button>
</body>
</html>