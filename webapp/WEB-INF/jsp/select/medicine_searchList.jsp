<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>검색 페이지</title>
<script src="<c:url value="/js/jquery-3.1.1.min.js"/>"></script>
</head>
<script type="text/javascript">
	$(document).ready(function(){
		var regExp = /\s/g;
		
		$("#medicine_button").click(function(){
			if($("#medicineCode").val()=="" || regExp.test($("#medicineCode").val())){
				alert("약품코드를 입력하세요.(공백X)");
				return false;
			}

		});
		
		$("#atcByMedicine_button").click(function(){
			if($("#medicineName").val()=="" || regExp.test($("#medicineName").val())){
				alert("약품이름을 입력하세요.(공백X)");
				return false;
			}

		});
		
		$("#atc_button").click(function(){
			if($("#atc").val()=="" || regExp.test($("#atc").val())){
				alert("ATC코드를 입력하세요.(공백X)");
				return false;
			}

		});
		
	});  
	   
</script>
   

<body>

<form action="<c:url value="/medicine/searchMedicine"/>" id=medicineSearch method="POST">
<input type='text' name='medicineCode' id="medicineCode">
<c:out value="약품코드로 약품정보 검색"></c:out>
<button id='medicine_button'>검색</button><br/>
</form>




<br/><br/><br/><br/><br/><br/><br/><br/><br/>

<form action="<c:url value="/medicine/searchAtc"/>" id=atcSearch method="POST">
<input type='text' name='atc' id="atc">
<c:out value="ATC코드로 ATC성분 검색"></c:out>
<button id='atc_button'>검색</button>
</form>

<br/><br/><br/><br/><br/><br/><br/><br/><br/>

<form action="<c:url value="/medicine/searchATCByMedicineName"/>" id=atcByMedicineSearch method="POST">
<input type='text' name='medicineName' id="medicineName">
<c:out value="약품명으로 ATC코드 검색"></c:out>
<button id='atcByMedicine_button'>검색</button><br/>
</form>

</html>