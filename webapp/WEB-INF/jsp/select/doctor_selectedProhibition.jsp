<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>상위 회피 ATC 입력 페이지</title>
<script src="<c:url value="/js/jquery-3.1.1.min.js"/>"></script>
</head>

<script type="text/javascript">
$(document).ready(function(){
	var regExp = /\s/g;
	var prohibitionList = "";
	$("#prohibitionList").children().each(function() {
		prohibitionList += $(this).val()+" ";
	});
	
	$("#logout").click(function(){
		
		window.location.href="<c:url value="/user/logout"/>";
		
	});

	$("#next").click(function(){
		var atcList = [];
		var number = $("#atcTable tr").length
		atcList.push($("#atcTable tr td:nth-child(1)").text() + " ");
		var result = atcList.toString();
		$("#atcList").val(result);
	});
	
	$("#delete").click(function(){
		$("#atcTable tr ").has('input[name="checkATC"]:checked').remove();
		return false;
	});
	
	$("#input").click(function(){
	
		if($("#upperATC").val() == "" || regExp.test($("#upperATC").val())){
			alert("상위ATC를 입력하세요!(공백X)");
			$("#upperATC").focus();
			return false;
		}
		
		var upperATC= $("#upperATC").val();
		
		if($("#atcTable").text().indexOf(upperATC) != -1){
			alert("이미 목록에 추가된 상위 ATC입니다.");
			$("#upperATC").val("");
			return false;
		}
		
	
		var request = $.ajax({
			type: "POST",
			url: "<c:url value="/prescription/upperATC_check"/>",
			data: {upperATC:upperATC,
					prohibitionList:prohibitionList
					},
			dataType: "json",
			success: function(jsonObj){
				
				if(jsonObj.msg != null)
				{
					alert(jsonObj.msg);	
					$("#upperATC").val("");
				}
				else if(jsonObj.isExist == false){
					alert("입력하신 ATC코드는 존재하지 않습니다.");
					$("#upperATC").val("");
				}
				else{
					var time = new Date().toLocaleTimeString();
					$("#atcTable > tbody:last").append("<tr><td>"+jsonObj.atcCode+"</td><td>"+jsonObj.atcName+"</td><td>"
							+'<input type="checkbox" name="checkATC"/>' + "</td></tr>");
					$("#upperATC").val("");

					
				}
				
			}
			
		});
				event.preventDefault();
	});
	
});

		
		
</script>
	

<body>
<form id="upperAtcform" action="<c:url value="/prescription/insertAlergySecond"/>" method="post">
		상위 ATC 코드:<Input type="text" name="upperATC" id="upperATC"/>
		<Button name="input" id="input">입력하기</Button>
		<Button name="delete" id="delete">삭제하기</Button>
		<br>
		<br>
		<table id="atcTable" border="1" cellspacing="0">
			<tr>
				<th>ATC코드</th>
				<th>ATC성분</th>
				<th>선택(삭제할 목록)</th>
			</tr>
			<tbody></tbody>
		</table>
		<br>
		<div id="prohibitionList">
		<c:set var="cnt" value="1"/>
		<c:forEach items="${prohibitionList }" var="prohibition">
			<Input type="hidden" id="prohibition<c:out value="${cnt }"/>" name="prohibition<c:out value="${cnt }"/>" value="<c:out value="${prohibition.prohibitedMedicine.code }"/>">
			<c:set var="cnt" value="${cnt+1 }"/>
		</c:forEach>
		</div>
		<Input type="hidden" id="atcList" size=10 name="atcList">
		<Button name="next" id="next">다음단계</Button>
</form>
<br><br>

<Button name="logout" id="logout">로그아웃</Button>

</body>
</html>