<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="EUC-KR"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>

<title>user list</title>
</head>
<script src="<c:url value="/js/jquery-3.1.1.min.js"/>"></script>
<script type="text/javascript">
	var regExp = /\s/g;
	<c:set value="${user }" var="user"/>
	<c:if test="${user == null}">
		$(document).ready(function(){
			alert("로그인이 필요한 페이지입니다.");
			window.location.href="<c:url value="/index.jsp"/>";
		});
	</c:if>
	
	$(document).ready(function(){
		$("#logout").click(function(){
			
			window.location.href="<c:url value="/user/logout"/>";
			
		});
		
		$("#changeButton").click(function(){
			alert("변경사항이 저장되었습니다.");
			$("#changeInform").submit();
		});
		
			$("#searchButton").click(function(){
				if($("#search").val() =="" || regExp.test($("#search").val())){
					alert("검색어를 바르게 입력하세요(공백X).");
					return false;
				}
				$("#searchForm").submit();
			});
		
	});
	
	
</script>
<body>

	<form id="changeInform" action="<c:url value="/user/accountManagementFinal"/>" method="post">	
	<H4>유저 리스트</H4>
	<table border=1>
		<TR>
			<TD width=40>순번</TD>
			<TD width=150>아이디</TD>
			<TD width=80>이름</TD>
			<TD width=40>성별</TD>
			<TD width=150>충북대코드</TD>
			<TD width=150>가입날짜</TD>
			<TD width=100>비활성화 여부</TD>
			<TD width=80>삭제하기</TD>
		</TR>
		<c:set var="start" value="${paging.pg*paging.pageSize}"/>
		<c:set var="cnt" value="${paging.pg*paging.pageSize}"/>
		
		<c:forEach var="user" items="${userList}">
			<c:set var="cnt" value="${cnt+1 }"/>
		<c:choose>
		<c:when test="${user.userId == 'admin'}">
			<c:set var="cnt" value="${cnt-1 }"/>
		</c:when>
		<c:otherwise>
		<TR>
			<TD><c:out value="${cnt }"/></TD>
			<TD><c:out value="${user.userId }"/></TD> 
			<TD><c:out value="${user.name }"/></TD>
			<TD><c:out value="${user.gender }"/></TD>
			<TD><c:out value="${user.cbnuCode }"/></TD>
			<TD><fmt:formatDate value="${user.date }"  pattern="yyyy-MM-dd hh:mm"/></TD>
			<TD>
			<c:choose>
				<c:when test="${user.disable == 'N' }">
					N<input id='disable<c:out value="${cnt }"/>' type="radio" name='disable<c:out value="${cnt }"/>' value="N" checked>
					Y<input id='disable<c:out value="${cnt }"/>' type="radio" name='disable<c:out value="${cnt }"/>' value="Y">
				</c:when>
				<c:otherwise>
					N<input id='disable<c:out value="${cnt }"/>' type="radio" name='disable<c:out value="${cnt }"/>' value="N" >
					Y<input id='disable<c:out value="${cnt }"/>' type="radio" name='disable<c:out value="${cnt }"/>' value="Y" checked>				
				</c:otherwise>
			</c:choose>
			</TD>
			<TD><input name='delete<c:out value="${cnt}"/>' type="checkbox" value="1">
		</TR>
		</c:otherwise>
		</c:choose>
		</c:forEach>
	</table>
<c:if test="${paging.endPage-1 >= 0 }">	
	<c:choose>
		<c:when test="${paging.pg > 0 }">
			<a href="<c:url value="/user/accountManagement?pg=0"/>">◀◀</a>
			<a href="<c:url value="/user/accountManagement?pg=${paging.pg-1 }"/>">◀</a>
		</c:when>
		<c:otherwise>
			<c:out value="　　　"/>
		</c:otherwise>
	</c:choose>

	<c:forEach var="i" begin="${paging.startPage-1}" varStatus="end" end="${paging.endPage-1}">
		
		<c:choose>
			<c:when test="${i == paging.pg }">
				<c:out value="${i+1}"/>
			</c:when>
			<c:otherwise>
				<a href="<c:url value="/user/accountManagement?pg=${i}"/>"><c:out value="${i+1}"/></a>
			</c:otherwise>
		</c:choose>
	</c:forEach>
	
	<c:choose>
		<c:when  test="${paging.pg < paging.allPage-1 }">
			<a href="<c:url value="/user/accountManagement?pg=${paging.pg+1}"/>">▶</a>
			<a href="<c:url value="/user/accountManagement?pg=${paging.allPage-1 }"/>">▶▶</a>
		</c:when>
		<c:otherwise>
			<c:out value="　　　"/>
		</c:otherwise>
	</c:choose>
</c:if>
		<c:set var="i" value="${start}"/>
		<c:forEach var="user" items="${userList}">
			<c:set var="i" value="${i+1 }"/>
			<input type="hidden" name='userId<c:out value="${i}"/>' value="${user.userId }">
			
		</c:forEach>
		<input type="hidden" name="start" value="${start }">
		<input type="hidden" name="cnt" value="${i}">
	</form>
	<Button id="changeButton" name="changeButton">변경사항 적용하기</Button>
	<br><br>
	<form id="searchForm" action="<c:url value="/user/accountManagementSearch"/>" method="post">
		검색<input type="text" id="search" name="search">
		<select name="searchKind">
		<option value="id" selected>아이디로 검색</option>
		<option value="name">이름으로 검색</option>
		</select>
	</form>
	<Button id="searchButton" name="searchButton">검색하기</Button>
	<Button name="logout" id="logout">로그아웃</Button>	
	
</body>
</html>