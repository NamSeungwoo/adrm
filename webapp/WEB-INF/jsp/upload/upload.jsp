<%@page contentType="text/html; charset=UTF-8"%>
<html>
<head>
<title>update!!</title>
<script src="<c:url value="/js/jquery-3.1.1.min.js"/>"></script>
</head>
<script type="text/javascript">  

	$(document).ready(function(){
		$("#medicine_button").click(function(){			
			 
			if ($("#medicineFile").val() == "") {   
				  alert("Upload your file.");   
				  
				  return false;   
			 } 
			
			else if(!checkFileType($("#medicineFile").val()))
			{   
				alert("You should upload only excel file. ** .xls");   
			  
				return false;   
			}
				
		});
		
		$("#atc_button").click(function(){
			if ($("#atcFile").val() == "") {   
				  alert("Upload your file.");     
				  return false;   

			 } 
			
			else if(!checkFileType($("#atcFile").val()))
			{   

			alert("You should upload only excel file. ** .xls");   
				  

			return false;   

			}
		});
	});
	
	var checkFileType = function(filePath){   
		if (filePath.substring(filePath.length - 4).toLowerCase() == ".xls")
			return true;    
		else
			return false;
	}


</script>  

<body>

<H4>only input Database.xls file!</H4>
<form action="<c:url value="/medicine/update"/>" name="upload" method="POST" enctype="multipart/form-data">
<td><input id="medicineFile" name="medicineFile" type="file" size="20" align="absmiddle" />    </td>
<td><button id='medicine_button'>DataBase 등록</button>

</form>

<H4>only input atc.xls file!</H4>
<form action="<c:url value="/medicine/updateatc"/>" name="uploadAtc" method="POST" enctype="multipart/form-data">
<td><input id="atcFile" name="atcFile" type="file" size="20" align="absmiddle" />    </td>
<td><button id='atc_button'>ATC 등록</button>

</form>

</body>
</html>

 