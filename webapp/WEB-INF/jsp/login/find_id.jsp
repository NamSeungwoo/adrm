<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script src="<c:url value="/js/jquery-3.1.1.min.js"/>"></script>
<script type="text/javascript">
	$(document).ready(function(){
		$("#requestCheckId").click(function(){
							
			
			var cbnuCode= $("#cbnuCode").val();
			var request = $.ajax({
				type: "POST",
				url: "<c:url value="/user/requestCheckId"/>",
				data: {cbnuCode:cbnuCode},
				dataType: "json",
				success: function(jsonObj){
					if(jsonObj.isSuccess == true){		
						$("#cbnuCode_inform").text("입력하신 충북대 코드에 해당하는 아이디는 "+ jsonObj.msg+" 입니다.");
					}
					else{
						$("#cbnuCode_inform").text("입력하신 충북대 코드에 해당하는 아이디가 존재하지 않습니다.");
					}
					
				}
								
			});
						
						event.preventDefault();
		});
		
		$("#findPasswordBtn").click(function(){
			window.location.href="<c:url value="/user/findPassword"/>";
		});
		$("#backBtn").click(function(){
			window.location.href="<c:url value="/user/login"/>";
		});
		
	});
	
</script>
<title>아이디 찾기</title>
</head>
<body>
	<H4>충북대코드를 입력해주세요.</H4>

	<form id="findIdForm"  action="<c:url value="/user/membership_end"/>" method="post">	
		충북대학교코드: <input type="text" id="cbnuCode" name="cbnuCode" size="10">
		<div id="cbnuCode_inform"></div>
		<Button name="requestCheckId" id="requestCheckId">코드확인</Button>
		<br>
	</form>
		<Button name="findPasswordBtn" id="findPasswordBtn">비밀번호 찾기</Button>
		<Button name="backBtn" id="backBtn">로그인</Button>
	<p></p>
</body>
</html>