<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-KR">
<script src="<c:url value="/js/jquery-3.1.1.min.js"/>"></script>
<title></title>
</head>
<script type="text/javascript">
	$(document).ready(function(){
		$("#submitBtn").click(function(){
			if($("#userId").val() == ""){
				alert("아이디를 입력하세요.");
				return false;
			}
			if($("#password").val() == ""){
				alert("비밀번호를 입력하세요.");
				return false;
			}
			$("#loginform").submit();
		});
		$("#findIdBtn").click(function(){
			window.location.href="<c:url value="/user/findId"/>";
		})
		$("#findPasswordBtn").click(function(){
			window.location.href="<c:url value="/user/findPassword"/>";
		})
	});
</script>
<body>
	
		<c:if test="${param.disable == true }">
			비활성화 상태인 계정입니다. 로그인을 할 수 없습니다.
		</c:if>
		<c:if test="${param.wrongValue == true}">
			아이디 또는 비밀번호가 틀립니다.
		</c:if>
	
	<H4>로그인</H4>
	<form id="loginform" action="<c:url value="/user/login_end"/>"  method="post">
		아이디: <input type="text" name="userId" id="userId" size="10"><br>
		패스워드: <input type="password" name="password" id="password" size="10"><br>
		<input type="button" id="submitBtn" value="전송">
	</form>
	<br><input type="button" id="findIdBtn" value="아이디 찾기">
	<br><input type="button" id="findPasswordBtn" value="패스워드 찾기">
</body>
</html>