<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script src="<c:url value="/js/jquery-3.1.1.min.js"/>"></script>
<script type="text/javascript">
	$(document).ready(function(){
		var count = 0;
		$("#checkInform").click(function(){
			var count = 0;		
			var userId = $("#userId").val();
			var cbnuCode= $("#cbnuCode").val();
			var request = $.ajax({
				type: "POST",
				url: "<c:url value="/user/requestMail"/>",
				data: {
					userId:userId,
					cbnuCode:cbnuCode},
				dataType: "json",
				success: function(jsonObj){
					if(jsonObj.isSuccess == true){		
						$("#_user").val(jsonObj._user);
						$("#_cbnuCode").val(jsonObj._cbnuCode);
						$("#_email").val(jsonObj._email);
						count = 1;
						alert("아이디와 충북대코드를 확인하였습니다. 비밀번호를 메일로 전송 버튼을 눌러주세요.");
					}
					else{
						count = 0;
						alert("아이디와 충북대코드를 다시 한번 확인해주세요.");
						$("#userId").val("");
						$("#cbnuCode").val("");
						$("#userId").focus();
					}
					
				}
								
			});
						
						event.preventDefault();
		});
		
		$("#findIdBtn").click(function(){
			window.location.href="<c:url value="/user/findId"/>";
		});
		$("#backBtn").click(function(){
			window.location.href="<c:url value="/user/login"/>";
		});
		$("#requestMail").click(function(){
			if($("#userId").val() == ""){
				alert("아이디를 입력하세요");
				$("#userId").focus();
				return false;
			}
			if($("#cbnuCode").val() == ""){
				alert("충북대 코드를 입력하세요");
				$("#cbnuCode").focus();
				return false;
			}
		});
	});
	
</script>
<title>비밀번호 찾기</title>
</head>
<body>
	<H4>아이디와 충북대코드를 입력해주세요.</H4>

	<form id="findIdForm"  action="<c:url value="/user/mailSender"/>" method="post">	
		아이디: <input type="text" name="userId" id="userId" size="10">
		충북대학교코드: <input type="text" id="cbnuCode" name="cbnuCode" size="10">

		<input type="hidden" name="_user" id="_user">
		<input type="hidden" name="_cbnuCode" id="_cbnuCode">
		<input type="hidden" name="_email" id="_email">
		<Button name="checkInform" id="checkInform">아이디와 충북대코드 확인</Button>
		<Button name="requestMail" id="requestMail">비밀번호를 메일로 전송</Button>
		<br>
	</form>
		<Button name="findPasswordBtn" id="findIdBtn">아이디 찾기</Button>
		<Button name="backBtn" id="backBtn">로그인</Button>
	
	
</body>
</html>