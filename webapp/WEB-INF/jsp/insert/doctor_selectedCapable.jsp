<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="EUC-KR"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>

<title>마지막 페이지</title>
<script src="<c:url value="/js/jquery-3.1.1.min.js"/>"></script>
</head>

<script type="text/javascript">
$(document).ready(function(){

	var confirmCnt = 0;
	var upperList = "";
	var prohibitionList = "";
	var capableList = "";
	$("#upperList").children().each(function() {
		upperList += $(this).val()+" ";
	});
	$("#prohibitionList").children().each(function() {
		prohibitionList += $(this).val()+" ";
	});
	$("#capableList").children().each(function() {
		capableList += $(this).val()+" ";
	});
	
	$("#logout").click(function(){
		
		window.location.href="<c:url value="/user/logout"/>";
		
	});

	$("#finalConfirm").click(function(){
		if(confirmCnt==0){
			alert("점검하기를 눌러주세요");
			return false;
		}
			
	});
	
	$("#finalCancel").click(function(){
		window.location.href="<c:url value="/prescription/finalCancel"/>";
		return false;
	});
	
	$("#confirm").click(function(){
	
		var request = $.ajax({
			type: "POST",
			url: "<c:url value="/prescription/final_check"/>",
			dataType: "json",
			data:{
				prohibitionList:prohibitionList,
				upperList:upperList,
				capableList:capableList
			},
			success: function(jsonObj){
				if(jsonObj.resultOk == true){
					alert("이상이 없습니다. 처방하기를 눌러주세요.");
					confirmCnt = 1;
				}
				else{
					if(jsonObj.msg == "PROHIBITION_SAME_UPPERATC"){
						alert("금기약제의 ATC코드와 회피상위 ATC코드가 같은 목록이 있습니다.");
					}
					else if(jsonObj.msg == "PROHIBITION_SAME_CAPABLE"){
						alert("금기약제의 ATC코드와 복용가능 약제의 ATC코드가 같은 목록이 있습니다.");
					}
					else if(jsonObj.msg == "UPPERATC_NOT_IN_PROHIBITION"){
						alert("금기약제의 회피상위 ATC코드가 목록에 없습니다.")
					}
					else if(jsonObj.msg == "UPPERATC_NOT_IN_CAPABLE"){
						alert("복용약제의 회피상위 ATC코드가 목록에 없습니다.")
					}
					else if(jsonObj.msg == "CAPABLE_SAME_UPPERATC"){
						alert("복용약제의 ATC코드와 회피상위 ATC코드가 같은 목록이 있습니다.")
					}
					confirmCnt = 0;
					
				}
				
			}
			
		});
				event.preventDefault();
	});
});
		
</script>
<H3>최종처방</H3>
<br><br>
<body>
<Button name="logout" id="logout">로그아웃</Button>
<br><br><br>
<form id="finalPrescription" action="<c:url value="/prescription/insertAlergyFinal"/>" method="POST">
<Button name="confirm" id="confirm">점검하기</Button>
<Button name="finalConfirm" id="finalConfirm">처방하기</Button><br><br>
<Button name="finalCancel" id="finalCancel">다시하기</Button>
	<H4>금지약제 리스트</H4>
	<table border=1>
		<TR>
			<TD width=40>순번</TD>
			<TD width=40>약제코드</TD>
			<TD width=500>약제이름</TD>
			<TD width=80>ATC코드</TD>
			<TD width=300>ATC성분</TD>
		</TR>
		<c:set var="cnt" value="0"/>
		<c:forEach var="prohibition" items="${prohibitionList}">
		<c:set var="cnt" value="${cnt+1 }"/>
		<TR>
			<TD><c:out value="${cnt }"/></TD>
			<TD><c:out value="${prohibition.prohibitedMedicine.code }"/></TD>
			<TD><c:out value="${prohibition.prohibitedMedicine.fullName }"/></TD>
			<TD><c:out value="${prohibition.prohibitedMedicine.atc.atcCode }"/></TD>
			<TD><c:out value="${prohibition.prohibitedMedicine.atc.atcName }"/></TD>
		</TR>
		</c:forEach>
	</table>
	<br><br>
	
	<H4>회피상위ATC 리스트</H4>
	<table border=1>
		<TR>
			<TD width=40>순번</TD>
			<TD width=80>ATC코드</TD>
			<TD width=300>ATC성분</TD>
		</TR>
		<c:set var="cnt" value="0"/>
		<c:forEach var="upper" items="${upperList}">
		<c:set var="cnt" value="${cnt+1 }"/>
		<TR>
			<TD><c:out value="${cnt }"/></TD>
			<TD><c:out value="${upper.atc.atcCode }"/></TD>
			<TD><c:out value="${upper.atc.atcName }"/></TD>
		</TR>
		</c:forEach>
	</table>
	<br><br>
	
	<H4>복용가능약제 리스트</H4>
	<table border=1>
		<TR>
			<TD width=40>순번</TD>
			<TD width=40>약제코드</TD>
			<TD width=500>약제이름</TD>
			<TD width=80>ATC코드</TD>
			<TD width=300>ATC성분</TD>
		</TR>
		<c:set var="cnt" value="0"/>
		<c:forEach var="capable" items="${capableList}">
		<c:set var="cnt" value="${cnt+1 }"/>
		<TR>
			<TD><c:out value="${cnt }"/></TD>
			<TD><c:out value="${capable.capableMedicine.code }"/></TD>
			<TD><c:out value="${capable.capableMedicine.fullName }"/></TD>
			<TD><c:out value="${capable.capableMedicine.atc.atcCode }"/></TD>
			<TD><c:out value="${capable.capableMedicine.atc.atcName }"/></TD>
		</TR>
		</c:forEach>
	</table>
	
	<div id="prohibitionList">
	<c:set var="cnt" value="1"/>
	<c:forEach items="${prohibitionList }" var="prohibition">
		<Input type="hidden" id="prohibition<c:out value="${cnt }"/>" name="prohibition<c:out value="${cnt }"/>" value="<c:out value="${prohibition.prohibitedMedicine.code }"/>">
		<c:set var="cnt" value="${cnt+1 }"/>
	</c:forEach>
	</div>
		
	<div id="upperList">
	<c:set var="cnt" value="1"/>
	<c:forEach items="${upperList }" var="upper">
		<Input type="hidden" id="upper<c:out value="${cnt }"/>" name="upper<c:out value="${cnt }"/>" value="<c:out value="${upper.atc.atcCode }"/>">
		<c:set var="cnt" value="${cnt+1 }"/>
	</c:forEach>
	</div>
	
	<div id="capableList">
	<c:set var="cnt" value="1"/>
	<c:forEach items="${capableList }" var="capable">
		<Input type="hidden" id="capable<c:out value="${cnt }"/>" name="capable<c:out value="${cnt }"/>" value="<c:out value="${capable.capableMedicine.code }"/>">
		<c:set var="cnt" value="${cnt+1 }"/>
	</c:forEach>
	</div>
		
</form>
	
	
	
</body>
</html>