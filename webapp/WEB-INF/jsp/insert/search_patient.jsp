<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>환자 검색 페이지</title>
<script src="<c:url value="/js/jquery-3.1.1.min.js"/>"></script>
</head>

<script type="text/javascript">
<c:set value="${user }" var="user"/>
	<c:if test="${user == null}">
	$(document).ready(function() {
		alert("로그인이 필요한 페이지입니다.");
		window.location.href = "<c:url value="/index.jsp"/>";
	});
	</c:if>
	
$(document).ready(function(){
	var regExp = /\s/g;
	var patientListValues = [];
	$("#patientList").children().each(function(){
		patientListValues.push($(this).val());
	});
	patientListValues = patientListValues.toString();
	
	$("#logout").click(function(){
		
		window.location.href="<c:url value="/user/logout"/>";
		
	});
	
	$("#next").click(function(){
		if($("#registration").val()=="" ){
			alert("환자를 먼저 검색해주세요.")
			return false;
		}
		
		if(count == 0)
		{
			alert("충북대코드를 확인해주세요.");
			return false;
		}
		
	});
	
	$("#search_ok").click(function(){
	
		var count = 0;
		
		if($("#search").val() == "" || 	regExp.test($("#search").val())){
			count = 0;
			alert("충북대코드를 입력하세요!(공백X)");
			$("#search").focus();
			return false;
		}
		
		$("#UserTable").find("tr:gt(0)").remove();
		
		var search= $("#search").val();
	
		var request = $.ajax({
			type: "POST",
			url: "<c:url value="/prescription/searchPatientResult"/>",
			data: {search:search,
					patientListValues:patientListValues},
			dataType: "json",
			success: function(jsonObj){
				if(jsonObj.isExist == false){
					count = 0;					
					alert("입력하신 충북대코드에 해당하는 환자가 없습니다.");		
				}
				else if(jsonObj.isDupl == true){
					count = 0;
					alert("이미 목록에 등록되어 있는 환자입니다.");
				}
				else{
					count = 1;
					
					$("#UserTable > tbody:last").append("<tr><td>"+jsonObj.patientId+"</td><td>"+jsonObj.patientName+"</td><td>"
							+jsonObj.patientGender+"</td><td>"+jsonObj.patientCode+"</td></tr>");
					$("#registration").val(jsonObj.patientId);	
					$("#search").val(jsonObj.patientCode);
				}
				
			}
			
		});
				event.preventDefault();
	});

});

</script>
	

<body>

<form id="loginform" action="<c:url value="/prescription/searchPatientRegistration"/>"  method="post">
		충북대코드:<Input type="text" name="search" id="search"/>
		<Button name="search_ok" id="search_ok">검색하기</Button>	
		<Button name="next" id="next">환자등록</Button>
		<br>
		<br>
		<table id="UserTable" border="1" cellspacing="0">
			<tr>
				<th>아이디</th>
				<th>이름</th>
				<th>성별</th>
				<th>CBNU코드</th>
			</tr>
			<tbody></tbody>
		</table>
		<br>
		<Input type="hidden" name="registration" id="registration">
		
		<div id="patientList">
		<c:set var="cnt" value="1"/>
		<c:forEach items="${patientList }" var="patient">
			<Input type="hidden" id="patient<c:out value="${cnt }"/>" name="patient<c:out value="${cnt }"/>" value="<c:out value="${patient}"/>">
			<c:set var="cnt" value="${cnt+1 }"/>
		</c:forEach>
		</div>
		
</form>

<Button name="logout" id="logout">로그아웃</Button>

</body>
</html>