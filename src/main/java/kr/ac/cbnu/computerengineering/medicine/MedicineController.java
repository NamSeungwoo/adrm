package kr.ac.cbnu.computerengineering.medicine;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Enumeration;
import java.util.List;
import java.util.Vector;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;

import com.oreilly.servlet.MultipartRequest;
import com.oreilly.servlet.multipart.DefaultFileRenamePolicy;

import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;
import kr.ac.cbnu.computerengineering.common.datatype.MedicineDataType;
import kr.ac.cbnu.computerengineering.common.datatype.SearchParam;
import kr.ac.cbnu.computerengineering.common.managers.IMedicineManager;
import kr.ac.cbnu.computerengineering.medicine.manager.MedicineManagerImpl;

/**
 * Servlet implementation class MedicienListServlet
 */

public class MedicineController extends HttpServlet {
	private static final long serialVersionUID = 1L;
    private IMedicineManager medicineManager;
    private static final int MEDICINE = 0;
    private static final int ATC = 1;
    /**
     * @see HttpServlet#HttpServlet()
     */
    public MedicineController() {
        super();
        // TODO Auto-generated constructor stub
        this.medicineManager = new MedicineManagerImpl();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//response.getWriter().append("Served at: ").append(request.getContextPath());
		this.checkURL(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		this.checkURL(request, response);
	}
	
	private void checkURL(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String url = request.getRequestURI();
		url = url.replace("/ADRM/medicine/", "");
		
		if(url.equals("searchMedicine")){
			this.medicineSearchRequest(request, response);
		}
		else if(url.equals("searchAtc")){
			this.atcSearchRequest(request,response);
		}
		else if(url.equals("searchATCByMedicineName")){
			this.atcSearchByMedicineRequest(request,response);
		}
		else if (url.contains("search")){
			this.searchPageRequest(request, response);
		}
		else if (url.contains("medicineManagement")){
			this.medicineManagementRequest(request,response);
		}
		else if(url.contains("list")) {
			this.medicineListRequest(request, response);
		} 
		else if(url.contains("upload")) {
			this.medicineUploadRequest(request,response);
		} 
		else if(url.equals("update")) {
			this.CheckDB(request, response, MEDICINE);
		} 
		else if(url.equals("updateatc")){
			this.CheckDB(request, response, ATC);
		}
	}
	private void medicineManagementRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
		RequestDispatcher dispatcher = request.getRequestDispatcher("/medicine/upload");
		dispatcher.forward(request, response);
	}
	private void atcSearchByMedicineRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{

		request.setCharacterEncoding("UTF-8");
		String name = (String) request.getParameter("medicineName");
		SearchParam param = new SearchParam();
		param.setParam(name);
		List<MedicineDataType> medicineDataType = this.medicineManager.selectATCByMedicineName(param);

		request.setAttribute("MEDICINE_ATC_LIST", medicineDataType);
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/select/medicine_atc_list.jsp");
		dispatcher.forward(request, response);
	}
	private void CheckDB(HttpServletRequest request, HttpServletResponse response, int type) throws ServletException, IOException{
		if(type == MEDICINE){
			if(checkDataBaseDB() == true){
				this.medicineReUpdateRequest(request, response, MEDICINE);
			}
			else{
				this.medicineUpdateRequest(request, response, MEDICINE);  // DataBase.xls
			}
		}
		else if(type == ATC){
			if(checkAtcDB() == true){
				this.medicineReUpdateRequest(request, response, ATC);
			}
			else{
				this.medicineUpdateRequest(request,response, ATC); // Atc.xls
			}
		}
	}
	
	private void searchPageRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{	
		
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/select/medicine_searchList.jsp");
		dispatcher.forward(request, response);  //리스트형식으로 보여주는 부분		
	}

	private void medicineSearchRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
		
		SearchParam param = new SearchParam();
		param.setParam("medicineCode");		
		String code = request.getParameter("medicineCode");
		param.setId(code);
		List<MedicineDataType> result = this.medicineManager.getMedicineList(param);
		request.setAttribute("MEDICINE_LIST", result);
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/select/medicine_list.jsp");
		dispatcher.forward(request, response);  //리스트형식으로 보여주는 부분
	}

	
	private void atcSearchRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
		
		SearchParam param = new SearchParam();
		param.setParam("atc");		
		String code = request.getParameter("atc");
		param.setId(code);
	
		List<MedicineDataType> result = this.medicineManager.getMedicineList(param);
		request.setAttribute("MEDICINE_ATC_LIST", result);
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/select/medicine_atc_list.jsp");
		dispatcher.forward(request, response);  //리스트형식으로 보여주는 부분		
	}
	
	private void deleteExcel(){
		this.medicineManager.deleteExcel();
	}
	
	private void deleteExcelAtc(){
		this.medicineManager.deleteExcelAtc();
	}
	
	private boolean checkDataBaseDB(){
		if(this.medicineManager.checkDataBase() > 0)
		{
			return true;
		}
		return false;
	}
	
	private boolean checkAtcDB()
	{
		if(this.medicineManager.checkAtc() > 0)
		{
			return true;
		}
		return false;
	}
	
	private void medicineReUpdateRequest(HttpServletRequest request, HttpServletResponse response, int checkFileNumber) throws ServletException, IOException{
		
		if(checkFileNumber == MEDICINE){
			deleteExcel();
			medicineUpdateRequest(request, response, 0);
		}
		else if(checkFileNumber == ATC){
			deleteExcelAtc();
			medicineUpdateRequest(request, response, 1);
		}
		
	}
	
	private void medicineUploadRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/upload/upload.jsp");
		dispatcher.forward(request, response);
	}
	
	private void medicineListRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		SearchParam param = new SearchParam();
		List<MedicineDataType> result = this.medicineManager.getMedicineList(param);
		request.setAttribute("MEDICINE_LIST", result);
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/select/medicine_list.jsp");
		dispatcher.forward(request, response);
		
	}
	
	private void medicineUpdateRequest(HttpServletRequest request, HttpServletResponse response, int checkFileNumber) throws ServletException, IOException {
		
		String savePath = request.getRealPath("/")+"upload"; // 저장할 디렉토리   
        int sizeLimit = 30 * 1024 * 1024 ; // 용량제한   
        String formName = "";   
        String fileName = "";   
        Vector vFileName = new Vector();   
        Vector vFileSize = new Vector();   
        String[] aFileName = null;   
        String[] aFileSize = null;   
        long fileSize = 0;   
        MultipartRequest multi = new MultipartRequest(request, savePath, sizeLimit,"UTF-8", new DefaultFileRenamePolicy());   
        Enumeration formNames = multi.getFileNames();    
        
        while (formNames.hasMoreElements()) {    
        	formName = (String)formNames.nextElement();    
        	fileName = multi.getFilesystemName(formName);    
         
        	if(fileName != null)  // 파일이 업로드 되면
        	{      
        		fileSize = multi.getFile(formName).length();   
        		vFileName.addElement(fileName);   
        		vFileSize.addElement(String.valueOf(fileSize));    
        	}    
     
       	}   
        
        aFileName = (String[])vFileName.toArray(new String[vFileName.size()]);   
        aFileSize = (String[])vFileSize.toArray(new String[vFileSize.size()]);    
        Workbook workbook = null;
        
		try {
			workbook = Workbook.getWorkbook(new java.io.File(savePath + "/" + fileName));
		} catch (BiffException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}    

        Sheet sheet = workbook.getSheet(0);   
     
        int col = sheet.getColumns();  // 시트의 컬럼의 수를 반환한다.    
        int row = sheet.getRows();   // 시트의 행의 수를 반환한다.  
     
        request.setAttribute("workbook", workbook);
        request.setAttribute("row", row);
        request.setAttribute("col", col); ///WEB-INF/jsp/upload/excel_insert
        request.setAttribute("aFileName", aFileName);
        request.setAttribute("aFileSize", aFileSize);
        
        // DB에 excel파일 입력
        try{
        	if(checkFileNumber == 0)
        		this.insertExcelFile(row, col, savePath, fileName);
        	else if(checkFileNumber == 1)
        		this.insertExcelAtcFile(row, col, savePath, fileName);
        }
        catch(Exception e){
        	if(checkFileNumber == 0)
        		this.medicineManager.deleteDataBaseBlank();
        	if(checkFileNumber == 1)
        		this.medicineManager.deleteAtcBlank();
        	e.printStackTrace();
        }
        finally{	
        	RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/upload/excel_insert.jsp");
            dispatcher.forward(request, response);
        }
        
	}

	private void insertExcelFile(int row, int col, String savePath, String fileName) {
	  	
    	try{
    		FileInputStream input = new FileInputStream(""+savePath+"\\"+fileName);
    		POIFSFileSystem fs = new POIFSFileSystem(input);
    		HSSFWorkbook wb = new HSSFWorkbook(fs);
    		HSSFSheet sheet2 = wb.getSheetAt(0);
    		Row row2;
    		MedicineDataType medicineDataType = new MedicineDataType();

    		for( int i=1; i<row;i++){
    			row2 = sheet2.getRow(i);
    			DataFormatter formatter = new DataFormatter();
    			Cell cell = sheet2.getRow(i).getCell(1);
    			
    			if(checkErrorCell(row2, i) == true )
    			{
    				medicineDataType.setCode(row2.getCell(0).getStringCellValue());
        			medicineDataType.setFullName(row2.getCell(1).getStringCellValue());
        			medicineDataType.setName(row2.getCell(2).getStringCellValue());
        			medicineDataType.getAtc().setAtcCode(row2.getCell(3).getStringCellValue());
        			medicineDataType.getAtc().setAtcName(row2.getCell(4).getStringCellValue());
    				this.medicineManager.uploadExcel_error(medicineDataType);
    				continue;
    			}
    			if(row2.getCell(0).getStringCellValue().equals(""))
    				continue;
    			
    			medicineDataType.setCode(row2.getCell(0).getStringCellValue());
    			medicineDataType.setFullName(row2.getCell(1).getStringCellValue());
    			medicineDataType.setName(row2.getCell(2).getStringCellValue());
    			medicineDataType.getAtc().setAtcCode(row2.getCell(3).getStringCellValue());
    			medicineDataType.getAtc().setAtcName(row2.getCell(4).getStringCellValue());
    				
    			this.medicineManager.uploadExcel(medicineDataType);
    						
    		}

    		
    		input.close();

    	}
    	catch(IOException ioe){	
    		ioe.printStackTrace();
    	}
    	finally{}
	}
	
	private void insertExcelAtcFile(int row, int col, String savePath, String fileName) {
	  	
    	try{
    		FileInputStream input = new FileInputStream(""+savePath+"\\"+fileName);
    		POIFSFileSystem fs = new POIFSFileSystem(input);
    		HSSFWorkbook wb = new HSSFWorkbook(fs);
    		HSSFSheet sheet2 = wb.getSheetAt(0);
    		Row row2;
    		MedicineDataType medicineDataType = new MedicineDataType();

    		for( int i=1; i<row;i++){
    			row2 = sheet2.getRow(i);
 				DataFormatter formatter = new DataFormatter();
    			Cell cell = sheet2.getRow(i).getCell(1);
 				medicineDataType.getAtc().setAtcCode(row2.getCell(0).getStringCellValue());
  				medicineDataType.getAtc().setAtcName(row2.getCell(1).getStringCellValue());
    			this.medicineManager.uploadExcelAtc(medicineDataType);
    					
    		}
    				//this.medicineManager.deleteAtcBlank();
    		input.close();

    	}
    	catch(IOException ioe){
    		ioe.printStackTrace();
    	}
    	finally{}
	}
	
	private boolean checkErrorCell(Row row, int i){
		int a = 0;
		if(row.getCell(0).getCellType() == Cell.CELL_TYPE_ERROR)
		{
			row.getCell(0).setCellType(Cell.CELL_TYPE_STRING);
			a++;
		}
		if(row.getCell(1).getCellType() == Cell.CELL_TYPE_ERROR)
		{
			row.getCell(1).setCellType(Cell.CELL_TYPE_STRING);
			a++;
		}
		if(row.getCell(2).getCellType() == Cell.CELL_TYPE_ERROR)
		{
			row.getCell(2).setCellType(Cell.CELL_TYPE_STRING);
			a++;
		}
		if(row.getCell(3).getCellType() == Cell.CELL_TYPE_ERROR)
		{
			row.getCell(3).setCellType(Cell.CELL_TYPE_STRING);
			a++;
		}
		if(row.getCell(4).getCellType() == Cell.CELL_TYPE_ERROR)
		{
			row.getCell(4).setCellType(Cell.CELL_TYPE_STRING);
			a++;
		}
		
		if(a>0)
			return true;
		return false;
		
	}
}
