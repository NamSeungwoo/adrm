package kr.ac.cbnu.computerengineering.medicine.manager.dao;

import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;

import kr.ac.cbnu.computerengineering.common.datatype.MedicineDataType;
import kr.ac.cbnu.computerengineering.common.datatype.SearchParam;
import kr.ac.cbnu.computerengineering.common.managers.dao.IMedicineDao;
import kr.ac.cbnu.computerengineering.common.mybatis.Mybatis;

public class MedicineDaoImpl implements IMedicineDao {
	private SqlSessionFactory session;
	
	public MedicineDaoImpl() {
		// TODO Auto-generated constructor stub
		this.session = Mybatis.getSqlSessionFactory();
	}
	
	@Override
	public List<MedicineDataType> details(SearchParam param) {
		// TODO Auto-generated method stub
		SqlSession session = this.session.openSession();
		List<MedicineDataType> result = null;
		try {
			result = session.selectList("medicine.details",param);
		} finally {
			session.close();
		}
		
		return result;
	}
	
	public int uploadExcel(MedicineDataType param) {
		
		SqlSession session = this.session.openSession(true);
		int result = 0;
		try{
			result = session.insert("medicine.upload", param);
		} finally {
			session.close();
		}
		
		return result;
	}
	public int uploadExcel_error(MedicineDataType param){
		
		SqlSession session = this.session.openSession(true);
		int result = 0;
		try{
			result = session.insert("medicine.upload_error", param);
		} finally {
			session.close();
		}
		
		return result;
	}
	public int uploadExcelAtc(MedicineDataType param){
		SqlSession session = this.session.openSession(true);
		int result = 0;
		try{
			result = session.insert("medicine.uploadAtc", param);
		} finally {
			session.close();
		}
		
		return result;
	}
	public int deleteExcel(){
		SqlSession session = this.session.openSession(true);
		int result = 0;
		try{
			result = session.delete("medicine.delete");
		} finally {
			session.close();
		}
		
		return result;
	}
	public int deleteExcelAtc(){
		SqlSession session = this.session.openSession(true);
		int result = 0;
		try{
			result = session.delete("medicine.deleteAtc");
		} finally {
			session.close();
		}
		
		return result;
	}
	public int checkDataBase(){
		SqlSession session = this.session.openSession(true);
		int result = 0;
		try{
			result = session.selectOne("medicine.checkDataBase");
		} finally {
			session.close();
		}
		
		return result;
	}
	public int checkAtc(){
		SqlSession session = this.session.openSession(true);
		int result = 0;
		try{
			result = session.selectOne("medicine.checkAtc");
		} finally {
			session.close();
		}
		
		return result;
	}
	public int deleteDataBaseBlank(){
		SqlSession session = this.session.openSession(true);
		int result = 0;
		try{
			result = session.delete("medicine.deleteDataBaseBlank");
		} finally {
			session.close();
		}
		
		return result;
	}
	public int deleteAtcBlank(){
		SqlSession session = this.session.openSession(true);
		int result = 0;
		try{
			result = session.delete("medicine.deleteAtcBlank");
		} finally {
			session.close();
		}
		
		return result;
	}
	public List<MedicineDataType> selectATCByMedicineName(SearchParam param){
		SqlSession session = this.session.openSession();
		List<MedicineDataType> result = null;
		try{

			result = session.selectList("medicine.selectAtcByMedicineName", param);

		} finally {
			session.close();
		}
		
		return result;
	}
	
}
