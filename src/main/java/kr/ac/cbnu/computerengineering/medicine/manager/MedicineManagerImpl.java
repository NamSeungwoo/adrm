package kr.ac.cbnu.computerengineering.medicine.manager;

import java.util.List;

import kr.ac.cbnu.computerengineering.common.datatype.MedicineDataType;
import kr.ac.cbnu.computerengineering.common.datatype.SearchParam;
import kr.ac.cbnu.computerengineering.common.managers.IMedicineManager;
import kr.ac.cbnu.computerengineering.common.managers.dao.IMedicineDao;
import kr.ac.cbnu.computerengineering.medicine.manager.dao.MedicineDaoImpl;



public class MedicineManagerImpl implements IMedicineManager {
	private IMedicineDao medicineDao;
	
	public MedicineManagerImpl() {
		// TODO Auto-generated constructor stub
		this.medicineDao = new MedicineDaoImpl();
	}
	
	@Override
	public List<MedicineDataType> getMedicineList(SearchParam param) {
		// TODO Auto-generated method stub
		return this.medicineDao.details(param);
	}

	public int uploadExcel(MedicineDataType param){
		return this.medicineDao.uploadExcel(param);
	}
	public int uploadExcel_error(MedicineDataType param){
		return this.medicineDao.uploadExcel_error(param);
	}
	public int uploadExcelAtc(MedicineDataType param) {
		return this.medicineDao.uploadExcelAtc(param);
	}
	public int deleteExcel(){
		return this.medicineDao.deleteExcel();
	}
	public int deleteExcelAtc(){
		return this.medicineDao.deleteExcelAtc();
	}
	public int checkDataBase(){
		return this.medicineDao.checkDataBase();
	}
	public int checkAtc(){
		return this.medicineDao.checkAtc();
	}
	public int deleteDataBaseBlank(){
		return this.medicineDao.deleteDataBaseBlank();
	}
	public int deleteAtcBlank(){
		return this.medicineDao.deleteAtcBlank();
	}
	public List<MedicineDataType> selectATCByMedicineName(SearchParam param){
		return this.medicineDao.selectATCByMedicineName(param);
	}

}
