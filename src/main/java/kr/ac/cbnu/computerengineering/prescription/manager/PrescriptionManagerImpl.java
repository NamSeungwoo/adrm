package kr.ac.cbnu.computerengineering.prescription.manager;

import java.util.List;

import kr.ac.cbnu.computerengineering.common.datatype.AtcDataType;
import kr.ac.cbnu.computerengineering.common.datatype.CapableDataType;
import kr.ac.cbnu.computerengineering.common.datatype.MedicineDataType;
import kr.ac.cbnu.computerengineering.common.datatype.ProhibitionDataType;
import kr.ac.cbnu.computerengineering.common.datatype.RegistrationDataType;
import kr.ac.cbnu.computerengineering.common.datatype.SearchParam;
import kr.ac.cbnu.computerengineering.common.datatype.UpperDataType;
import kr.ac.cbnu.computerengineering.common.datatype.UserDataType;
import kr.ac.cbnu.computerengineering.common.managers.IPrescriptionManager;
import kr.ac.cbnu.computerengineering.common.managers.dao.IPrescriptionDao;
import kr.ac.cbnu.computerengineering.prescription.manager.dao.PrescriptionDaoImpl;

public class PrescriptionManagerImpl implements IPrescriptionManager {

	IPrescriptionDao prescriptionDao;
	
	public PrescriptionManagerImpl(){
		prescriptionDao = new PrescriptionDaoImpl();
	}
	
	public void insertProhibition(ProhibitionDataType prescriptionDataType){
		this.prescriptionDao.insertProhibition(prescriptionDataType);
	}
	public List<MedicineDataType> alergyBan(SearchParam param){
		
		MedicineDataType medicineDataType = this.prescriptionDao.detailAtcByCode(param);
		String first = Character.toString(medicineDataType.getAtc().getAtcCode().charAt(0));
		param.setAtcFirst(first);		
		return this.prescriptionDao.detailsMedicineByAtc(param);
	}
	public MedicineDataType selectMedicineByCode(SearchParam param){
		return this.prescriptionDao.selectMedicineByCode(param);
	}
	public RegistrationDataType selectRegiByParam(SearchParam param){
		return this.prescriptionDao.selectRegiByParam(param);
	}
	public MedicineDataType medicineCheck(SearchParam param){
		return this.prescriptionDao.medicineCheck(param);
	}
	public AtcDataType atcCheck(SearchParam param){
		return this.prescriptionDao.atcCheck(param);
	}
	public void insertUpperAtc(UpperDataType upperDataType){
		this.prescriptionDao.insertUpperAtc(upperDataType);
	}
	public void insertCapable(CapableDataType capableDataType){
		this.prescriptionDao.insertCapable(capableDataType);
	}
	public ProhibitionDataType selectProhibitionByCode(SearchParam param){
		return this.prescriptionDao.selectProhibitionByCode(param);
	}
	public ProhibitionDataType selectProhibitionMedicineByParams(SearchParam param){
		return this.prescriptionDao.selectProhibitionMedicineByParams(param);
	}
	public int confirmProhibitionByIdx(SearchParam param){
		return this.prescriptionDao.confirmProhibitionByIdx(param);
	}
	public int confirmUpperAtcByIdx(SearchParam param){
		return this.prescriptionDao.confirmUpperAtcByIdx(param);
	}
	public int confirmCapableByIdx(SearchParam param){
		return this.prescriptionDao.confirmCapableByIdx(param);
	}
	public void deleteProhibition(SearchParam param){
		this.prescriptionDao.deleteProhibition(param);
	}
	public void deleteUpperAtc(SearchParam param){
		this.prescriptionDao.deleteUpperAtc(param);
	}
	public void deleteCapable(SearchParam param){
		this.prescriptionDao.deleteCapable(param);
	}
	public List<ProhibitionDataType> selectProhibitionByIdx(SearchParam param){
		return this.prescriptionDao.selectProhibitionByIdx(param);
	}
	public List<UpperDataType> selectUpperAtcByIdx(SearchParam param){
		return this.prescriptionDao.selectUpperAtcByIdx(param);
	}
	public List<CapableDataType> selectCapableByIdx(SearchParam param){
		return this.prescriptionDao.selectCapableByIdx(param);
	}
	public List<RegistrationDataType> selectRegiDoctorByPatientId(UserDataType userDataType){
		return this.prescriptionDao.selectRegiDoctorByPatientId(userDataType);
	}
}
