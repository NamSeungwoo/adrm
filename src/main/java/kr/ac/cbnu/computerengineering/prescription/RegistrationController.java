package kr.ac.cbnu.computerengineering.prescription;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.json.simple.JSONObject;

import kr.ac.cbnu.computerengineering.common.datatype.RegistrationDataType;
import kr.ac.cbnu.computerengineering.common.datatype.SearchParam;
import kr.ac.cbnu.computerengineering.common.datatype.UserDataType;
import kr.ac.cbnu.computerengineering.common.managers.IRegistrationManager;
import kr.ac.cbnu.computerengineering.prescription.manager.RegistrationManagerImpl;

public class RegistrationController {

	private IRegistrationManager registrationManager;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public RegistrationController() {
		super();
		this.registrationManager = new RegistrationManagerImpl(); // or
																	// UserManagerImpl2();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	public List<RegistrationDataType> detailPatientList(UserDataType userDataType){
		return this.registrationManager.detailPatientList(userDataType);
	}
	public void deletePatientRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
		HttpSession session = request.getSession();
		UserDataType userDataType = (UserDataType) session.getAttribute("user");
		SearchParam param = new SearchParam();
		param.setId(userDataType.getUserId());
		
		if(request.getParameter("checkPatient") != null){
			String[] patientList = request.getParameterValues("checkPatient");
			for(String patient : patientList){
				param.setParam(patient);
				this.registrationManager.deletePatient(param);
			}
		}
		
		List<RegistrationDataType> patients = this.registrationManager.detailPatientList(userDataType);

		patients = this.registrationManager.detailPatientList(userDataType);
		request.setAttribute("patientList", patients);
		
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/select/doctor_patientList.jsp");
		dispatcher.forward(request, response);
	}
	
	public void searchPatientRegistrationRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
		HttpSession session = request.getSession();
		SearchParam param = new SearchParam();
		UserDataType userDataType = (UserDataType) session.getAttribute("user");
		param.setId(userDataType.getUserId());
		param.setParam((String) request.getParameter("registration"));
		List<RegistrationDataType> patients = this.registrationManager.detailPatientList(userDataType);

		int flag = 0;
		for(RegistrationDataType registration : patients){
			if(param.getParam().equals(registration.getPatient().getUserId())){
				flag = 1;
			}
		}

		if(flag == 0)
			this.registrationManager.insertRegistration(param);
		
		patients = this.registrationManager.detailPatientList(userDataType);
		request.setAttribute("patientList", patients);
		
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/select/doctor_patientList.jsp");
		dispatcher.forward(request, response);
	}
	
	public void searchPatientRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		

		List<String> myPatientList = new ArrayList<String>();
		String val=null;
		int i=1;
		while((val = (String) request.getParameter("unit"+i)) != null){
						
			myPatientList.add(val);
			i++;
		}
		
		request.setAttribute("patientList", myPatientList);
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/insert/search_patient.jsp");
		dispatcher.forward(request, response);
	}

	public void getRegistrationDataRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		HttpSession session = request.getSession();
		UserDataType userDataType = new UserDataType();
		userDataType.setUserId((String) session.getAttribute("patientId"));

		int registrationIdx = this.registrationManager.selectRegistrationIdxByPatientId(userDataType);
		session.setAttribute("registrationIdx", registrationIdx);

	}
	
	public void searchPatientResultRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		UserDataType patient = new UserDataType();
		SearchParam param = new SearchParam();

		
		JSONObject json = new JSONObject();
		response.setCharacterEncoding("UTF-8");
		
		param.setCbnuCode(request.getParameter("search"));
		
		System.out.println("request.getParameter patientListValues: " + request.getParameter("patientListValues"));
		
		patient = this.registrationManager.searchPatient(param); 


		//hey
		String patientList = request.getParameter("patientListValues");
		String[] patientIdList = null;
		
		patientIdList = patientList.split(",");
		
		
		boolean duplFlag = false;
		
		for(String unit : patientIdList){
			if(unit.equals(patient.getUserId())){
				duplFlag = true;
			}
			
			if(duplFlag == true)
				break;
			
		}
		

		if(duplFlag == true)
		{
			json.put("isDupl", true);
		}
		else{
			json.put("isDuple", false);
		}
		
		if (patient == null) {
			json.put("isExist", false);
			json.put("patient", null);

		} else {
			json.put("isExist", true);
			json.put("patientId",patient.getUserId());
			json.put("patientName",patient.getName());
			json.put("patientGender",patient.getGender());
			json.put("patientCode",patient.getCbnuCode()); 

		}
			
		System.out.println(json);

		PrintWriter out = response.getWriter();
		out.print(json);
	}

	public void logoutCheckRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession();

		session.setAttribute("user", null);

		RequestDispatcher dispatcher = request.getRequestDispatcher("/index.jsp");
		dispatcher.forward(request, response);
	}

	public void doctorDetailsRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		HttpSession session = request.getSession();
		UserDataType userDataType = (UserDataType) session.getAttribute("user");

		List<RegistrationDataType> patients = this.registrationManager.detailPatientList(userDataType);
		request.setAttribute("patientList", patients);

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/select/doctor_patientList.jsp");
		dispatcher.forward(request, response);
	}
}