<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-KR">
<script src="<c:url value="/js/jquery-3.1.1.min.js"/>"></script>
<title>atc list</title>
</head>
<script type="text/javascript">

</script>
<body>
	<H4>검색 결과</H4>
	<table border=1>
		<TR>
			<TD width=40>순번</TD>
			<TD width=100>약제코드</TD>
			<TD width=200>약제이름</TD>
			<TD width=40>ATC코드</TD>
			<TD width=40>ATC이름</TD>
		</TR>
		<c:set var="cnt" value="0"/>
		<c:forEach var="unit" items="${ATC_LIST}">
		<c:set var="cnt" value="${cnt+1 }"/>
		<TR>
			<TD><c:out value="${cnt }"/></TD>
			<TD><c:out value="${unit.code}"/></TD>
			<TD><c:out value="${unit.fullName }"/></TD>
			<TD><c:out value="${unit.atc.atcCode}"/></TD>
			<TD><c:out value="${unit.atc.atcName}"/></TD>
		</TR>
		</c:forEach>
	</table>

</body>
</html>