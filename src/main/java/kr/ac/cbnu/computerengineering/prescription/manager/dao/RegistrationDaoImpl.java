package kr.ac.cbnu.computerengineering.prescription.manager.dao;

import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;

import kr.ac.cbnu.computerengineering.common.datatype.RegistrationDataType;
import kr.ac.cbnu.computerengineering.common.datatype.SearchParam;
import kr.ac.cbnu.computerengineering.common.datatype.UserDataType;
import kr.ac.cbnu.computerengineering.common.managers.dao.IRegistrationDao;
import kr.ac.cbnu.computerengineering.common.mybatis.Mybatis;

public class RegistrationDaoImpl implements IRegistrationDao {

private SqlSessionFactory sqlSessionFactory;
	
	
	public RegistrationDaoImpl(){
		this.sqlSessionFactory = Mybatis.getSqlSessionFactory();
	}
	
	public List<RegistrationDataType> detailPatientList(UserDataType userDataType){
		SqlSession session = this.sqlSessionFactory.openSession();
		List<RegistrationDataType> result = null;
		try {
			result = session.selectList("registration.detailPatientListJoin",userDataType);
		} finally {
			session.close();
		}
		
		return result;
	}
	public UserDataType searchPatient(SearchParam param){
		SqlSession session = this.sqlSessionFactory.openSession();
		UserDataType result = null;
		try {
			result = session.selectOne("registration.searchPatient",param);
				
		} finally {
			session.close();
		}
		
		return result;
	}
	public int selectRegistrationIdxByPatientId(UserDataType userDataType){
		SqlSession session = this.sqlSessionFactory.openSession();
		int result = 0;
		try {
			result = session.selectOne("registration.searchIdxById",userDataType);
				
		} finally {
			session.close();
		}
		
		return result;
	}
	public void insertRegistration(SearchParam param){
		SqlSession session = this.sqlSessionFactory.openSession(true);
		try {
			session.selectOne("registration.insertRegistration",param);
				
		} finally {
			session.close();
		}
		
	}
	public void deletePatient(SearchParam param){
		SqlSession session = this.sqlSessionFactory.openSession(true);
		try {
			session.selectOne("registration.deleteRegistration",param);
				
		} finally {
			session.close();
		}
	}
}
