package kr.ac.cbnu.computerengineering.prescription.manager.dao;

import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;

import kr.ac.cbnu.computerengineering.common.datatype.AtcDataType;
import kr.ac.cbnu.computerengineering.common.datatype.CapableDataType;
import kr.ac.cbnu.computerengineering.common.datatype.MedicineDataType;
import kr.ac.cbnu.computerengineering.common.datatype.ProhibitionDataType;
import kr.ac.cbnu.computerengineering.common.datatype.RegistrationDataType;
import kr.ac.cbnu.computerengineering.common.datatype.SearchParam;
import kr.ac.cbnu.computerengineering.common.datatype.UpperDataType;
import kr.ac.cbnu.computerengineering.common.datatype.UserDataType;
import kr.ac.cbnu.computerengineering.common.managers.dao.IPrescriptionDao;
import kr.ac.cbnu.computerengineering.common.mybatis.Mybatis;

public class PrescriptionDaoImpl implements IPrescriptionDao {

	private SqlSessionFactory sqlSessionFactory;
	
	public PrescriptionDaoImpl(){
		sqlSessionFactory = Mybatis.getSqlSessionFactory();
	}
	
	public void insertProhibition(ProhibitionDataType prescriptionDataType){
		SqlSession session = this.sqlSessionFactory.openSession(true);
		try {
			session.insert("prescription.insertProhibition",prescriptionDataType);
		}
		finally {
			session.close();
		}
		
	}
	public MedicineDataType detailAtcByCode(SearchParam param){
		SqlSession session = this.sqlSessionFactory.openSession();
		MedicineDataType result = null;
		try {
			result = session.selectOne("prescription.detailAtcByCode",param);
		} finally {
			session.close();
		}
		return result;
	}
	public List<MedicineDataType> detailsMedicineByAtc(SearchParam param){
		SqlSession session = this.sqlSessionFactory.openSession();
		List<MedicineDataType> result = null;
		try {
			result = session.selectList("prescription.detailsMedicineByAtc",param);
		} finally {
			session.close();
		}
		return result;
	}
	public MedicineDataType selectMedicineByCode(SearchParam param){
		SqlSession session = this.sqlSessionFactory.openSession();
		MedicineDataType result = null;
		try {
			result = session.selectOne("prescription.selectMedicineByCode",param);		
		} finally {
			session.close();
		}
		return result;
	}
	public RegistrationDataType selectRegiByParam(SearchParam param){
		SqlSession session = this.sqlSessionFactory.openSession();
		RegistrationDataType result = null;
		try {
			result = session.selectOne("prescription.selectRegiByParam",param);
		} finally {
			session.close();
		}
		return result;
	}
	public MedicineDataType medicineCheck(SearchParam param){
		SqlSession session = this.sqlSessionFactory.openSession();
		MedicineDataType result = null;
		try{
			result = session.selectOne("prescription.selectMedicineByCode", param);
		} finally {
			session.close();
		}
		
		return result;
	}
	public AtcDataType atcCheck(SearchParam param){
		SqlSession session = this.sqlSessionFactory.openSession();
		AtcDataType result = null;
		try{
			result = session.selectOne("prescription.selectAtcCode", param);
		} finally {
			session.close();
		}
		
		return result;
	}
	public void insertUpperAtc(UpperDataType upperDataType){
		SqlSession session = this.sqlSessionFactory.openSession(true);
		try{
			session.insert("prescription.insertUpperAtc", upperDataType);
		} finally {
			session.close();
		}
		
	}
	public void insertCapable(CapableDataType capableDataType){
		SqlSession session = this.sqlSessionFactory.openSession(true);
		try{
			session.insert("prescription.insertCapable", capableDataType);
		} finally {
			session.close();
		}

	}
	public ProhibitionDataType selectProhibitionMedicineByParams(SearchParam param){
		SqlSession session = this.sqlSessionFactory.openSession();
		ProhibitionDataType result = null;
		try{
			result = session.selectOne("prescription.selectProhibitionIdxByParams", param);
		} finally {
			session.close();
		}
		
		return result;
	}
	public ProhibitionDataType selectProhibitionByCode(SearchParam param){
		SqlSession session = this.sqlSessionFactory.openSession();
		ProhibitionDataType result = null;
		try{
			result = session.selectOne("prescription.selectProhibitionMedicineByIdx", param);
		} finally {
			session.close();
		}
		
		return result;
	}
	public int confirmProhibitionByIdx(SearchParam param){
		SqlSession session = this.sqlSessionFactory.openSession();
		int result = 0;
		try{
			result = session.selectOne("prescription.confirmProhibitionByIdx", param);
		} finally {
			session.close();
		}
		
		return result;
	}
	public int confirmUpperAtcByIdx(SearchParam param){
		SqlSession session = this.sqlSessionFactory.openSession();
		int result = 0;
		try{
			result = session.selectOne("prescription.confirmUpperAtcByIdx", param);
		} finally {
			session.close();
		}
		
		return result;
	}
	public int confirmCapableByIdx(SearchParam param){
		SqlSession session = this.sqlSessionFactory.openSession();
		int result = 0;
		try{
			result = session.selectOne("prescription.confirmCapableByIdx", param);
		} finally {
			session.close();
		}
		
		return result;
	}
	
	public void deleteProhibition(SearchParam param){
		SqlSession session = this.sqlSessionFactory.openSession(true);
		try{
			session.delete("prescription.deleteProhibition", param);
		} finally {
			session.close();
		}
		
	}
	public void deleteUpperAtc(SearchParam param){
		SqlSession session = this.sqlSessionFactory.openSession(true);
		try{
			session.delete("prescription.deleteUpperAtc", param);
		} finally {
			session.close();
		}
	}
	public void deleteCapable(SearchParam param){
		SqlSession session = this.sqlSessionFactory.openSession(true);
		try{
			session.delete("prescription.deleteCapable", param);
		} finally {
			session.close();
		}
	}
	public List<ProhibitionDataType> selectProhibitionByIdx(SearchParam param){
		SqlSession session = this.sqlSessionFactory.openSession();
		List<ProhibitionDataType> result = null;
		try {
			result = session.selectList("prescription.selectProhibition",param);
		} finally {
			session.close();
		}
		return result;
	}
	public List<UpperDataType> selectUpperAtcByIdx(SearchParam param){
		SqlSession session = this.sqlSessionFactory.openSession();
		List<UpperDataType> result = null;
		try {
			result = session.selectList("prescription.selectUpperAtc",param);
		} finally {
			session.close();
		}
		return result;
	}
	public List<CapableDataType> selectCapableByIdx(SearchParam param){
		SqlSession session = this.sqlSessionFactory.openSession();
		List<CapableDataType> result = null;
		try {
			result = session.selectList("prescription.selectCapable",param);
		} finally {
			session.close();
		}
		return result;
	}
	public List<RegistrationDataType> selectRegiDoctorByPatientId(UserDataType userDataType){
		SqlSession session = this.sqlSessionFactory.openSession();
		List<RegistrationDataType> result = null;
		try {
			result = session.selectList("registration.detailDoctorListJoin",userDataType);
		} finally {
			session.close();
		}
		return result;
	}

}
