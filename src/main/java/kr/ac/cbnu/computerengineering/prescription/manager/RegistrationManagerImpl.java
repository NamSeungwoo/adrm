package kr.ac.cbnu.computerengineering.prescription.manager;

import java.util.List;

import kr.ac.cbnu.computerengineering.common.datatype.RegistrationDataType;
import kr.ac.cbnu.computerengineering.common.datatype.SearchParam;
import kr.ac.cbnu.computerengineering.common.datatype.UserDataType;
import kr.ac.cbnu.computerengineering.common.managers.IRegistrationManager;
import kr.ac.cbnu.computerengineering.common.managers.dao.IRegistrationDao;
import kr.ac.cbnu.computerengineering.prescription.manager.dao.RegistrationDaoImpl;

public class RegistrationManagerImpl implements IRegistrationManager {
	
	IRegistrationDao registrationDao;
			
	public RegistrationManagerImpl(){
		this.registrationDao = new RegistrationDaoImpl();
	}
	
	public List<RegistrationDataType> detailPatientList(UserDataType userDataType){
		return this.registrationDao.detailPatientList(userDataType);
	}
	public UserDataType searchPatient(SearchParam param){
		return this.registrationDao.searchPatient(param);
	}
	public int selectRegistrationIdxByPatientId(UserDataType userDataType){
		return this.registrationDao.selectRegistrationIdxByPatientId(userDataType);
	}
	public void insertRegistration(SearchParam param){
		this.registrationDao.insertRegistration(param);
	}
	public void deletePatient(SearchParam param){
		this.registrationDao.deletePatient(param);
	}
}
