package kr.ac.cbnu.computerengineering.prescription;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.json.simple.JSONObject;

import kr.ac.cbnu.computerengineering.common.datatype.AtcDataType;
import kr.ac.cbnu.computerengineering.common.datatype.CapableDataType;
import kr.ac.cbnu.computerengineering.common.datatype.MedicineDataType;
import kr.ac.cbnu.computerengineering.common.datatype.ProhibitionDataType;
import kr.ac.cbnu.computerengineering.common.datatype.RegistrationDataType;
import kr.ac.cbnu.computerengineering.common.datatype.SearchParam;
import kr.ac.cbnu.computerengineering.common.datatype.UpperDataType;
import kr.ac.cbnu.computerengineering.common.datatype.UserDataType;
import kr.ac.cbnu.computerengineering.common.managers.IPrescriptionManager;
import kr.ac.cbnu.computerengineering.common.util.Utils;
import kr.ac.cbnu.computerengineering.prescription.manager.PrescriptionManagerImpl;

public class PrescriptionController extends HttpServlet {
	
private static final long serialVersionUID = 1L;
	
	private IPrescriptionManager prescriptionManager;
    private RegistrationController registrationController;
    /**
     * @see HttpServlet#HttpServlet()
     */
    public PrescriptionController() {
        super();
        this.prescriptionManager = new PrescriptionManagerImpl();
        this.registrationController = new RegistrationController();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//response.getWriter().append("Served at: ").append(request.getContextPath());
		checkURL(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		checkURL(request, response);
	}
	private void checkURL(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String url = request.getRequestURI();
		url = url.replace("/ADRM/prescription/", "");
		if(url.contains("logout")){
			this.logoutCheckRequest(request, response);
		}
		else if(url.contains("patientSelect")){
			this.selectPatientRequest(request, response);
		}
		else if(url.contains("deletePatient")){
			this.registrationController.deletePatientRequest(request, response);
		}
		else if(url.contains("confirmProhibition")){
			this.confirmProhibitionRequest(request, response);
		}
		else if(url.equals("insertAlergyFinal")){
			this.insertAlergyFinalRequest(request, response);
		}
		else if(url.equals("insertAlergySecond")){
			this.insertAlergySecondRequest(request,response);
		}
		else if(url.equals("insertAlergyThird")){
			this.insertAlergyThirdRequest(request,response);
		}
		else if(url.equals("insertAlergyFirst")){
			this.insertAlergyFirstRequest(request, response);
		}
		else if(url.equals("patientDetail")){
			this.MyAlergyDetailRequest(request, response);
		}
		else if(url.contains("patientDetailAlergy")){
			this.patientDetailAlergyRequest(request, response);
		}
		else if(url.contains("doctorDetails")){
			this.registrationController.doctorDetailsRequest(request, response);
		}
		else if(url.equals("searchPatientResult")){
			this.registrationController.searchPatientResultRequest(request, response);
		}
		else if(url.equals("searchPatientRegistration")){
			this.registrationController.searchPatientRegistrationRequest(request, response);
		}
		else if(url.contains("searchPatient")){
			this.registrationController.searchPatientRequest(request, response);
		}	
		else if(url.contains("prohibition_check")){
			this.prohibitionCheckRequest(request, response);
		}
		else if(url.contains("doctorSelect")){
			this.selectDoctorRequest(request, response);
		}
		else if(url.contains("medicine_check")){
			this.medicineCheckRequest(request, response);
		}
		else if(url.contains("upperATC_check")){
			this.upperATCCheckRequest(request, response);
		}
		else if(url.contains("capable_check")){
			this.capableCheckRequest(request, response);
		}
		else if(url.contains("finalCancel")){
			this.finalCancelRequest(request, response);
		}
		else if(url.contains("final_check")){
			this.finalCheckRequest(request, response);
		}
	}
	
	private void medicineCheckRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setCharacterEncoding("UTF-8");
		SearchParam param = new SearchParam();
		JSONObject json = new JSONObject();
		param.setParam(request.getParameter("medicineCode"));
		MedicineDataType medicineDataType = this.prescriptionManager.selectMedicineByCode(param);
		if(medicineDataType == null)
		{
			json.put("isExist", false);
		}
		else{
			json.put("isExist", true);
			json.put("medicineCode", medicineDataType.getCode());
			json.put("medicineName", medicineDataType.getFullName());
			json.put("atcCode", medicineDataType.getAtc().getAtcCode());
			json.put("atcName", medicineDataType.getAtc().getAtcName());
		}		
		
		PrintWriter out = response.getWriter();
		out.print(json);
		
	}
	private void confirmProhibitionRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		HttpSession session = request.getSession();
		SearchParam param = new SearchParam();
		UserDataType user = (UserDataType) session.getAttribute("user");
		param.setId(user.getUserId());
		param.setParam(request.getParameter("doctorId"));

		param.setIdx(this.prescriptionManager.selectRegiByParam(param).getIdx());
		

		List<ProhibitionDataType> prohibitionList = this.prescriptionManager.selectProhibitionByIdx(param);
		List<UpperDataType> upperList = this.prescriptionManager.selectUpperAtcByIdx(param);
		List<CapableDataType> capableList = this.prescriptionManager.selectCapableByIdx(param);
		String registrationDate = this.prescriptionManager.selectRegiByParam(param).getRegistrationDate().toString();
		
		//List<String> inputDatas..
		
		//TODO
		
		
		List<MedicineDataType> inputMedicines = new ArrayList<MedicineDataType>();
		String[] medicineList = request.getParameter("medicineList").split(" ");
		List<MedicineDataType> containProhibition = new ArrayList<MedicineDataType>();
		List<MedicineDataType> containAvoidance = new ArrayList<MedicineDataType>();
		List<MedicineDataType> containCapable = new ArrayList<MedicineDataType>();

		
		String temp = null;

		// 처방전에 있는 약품들
		for(String unit : medicineList){
			temp = unit;
			param.setParam(temp);
			inputMedicines.add(this.prescriptionManager.medicineCheck(param));
			temp = null;
		}
		
		// 기존 처방받은 금지,회피상위,복용가능 약제리스트에서 처방받은 약품들 확인
		String unitAtc = null;		
		
		for(ProhibitionDataType prohibition: prohibitionList){
			temp = prohibition.getProhibitedMedicine().getAtc().getAtcCode();
			
			for(String unit: medicineList){
				param.setParam(unit);
				unitAtc = this.prescriptionManager.selectMedicineByCode(param).getAtc().getAtcCode();
				if(temp.equals(unitAtc)){
					containProhibition.add(this.prescriptionManager.medicineCheck(param));
				}
			}
		}
		

		for(UpperDataType upper: upperList){
			temp = upper.getAtc().getAtcCode();
			
			for(String unit: medicineList){
				param.setParam(unit);
				unitAtc = this.prescriptionManager.selectMedicineByCode(param).getAtc().getAtcCode();
				if(unitAtc.contains(temp)){
					containAvoidance.add(this.prescriptionManager.medicineCheck(param));
				}
				unitAtc = null;
			}
		}
		
		
		for(CapableDataType capable: capableList){
			temp = capable.getCapableMedicine().getAtc().getAtcCode();
			
			for(String unit: medicineList){
				param.setParam(unit);
				unitAtc = this.prescriptionManager.selectMedicineByCode(param).getAtc().getAtcCode();
				if(temp.equals(unitAtc)){
					containCapable.add(this.prescriptionManager.medicineCheck(param));
				}
			}
		}


		
		//분류한 것들 중에서 금지약제에 해당하는 것이 회피약제에도 들어있는지
		
		for(int i=0; i< containAvoidance.size();i++){
			temp = containAvoidance.get(i).getAtc().getAtcCode();
			for(MedicineDataType prohibition: containProhibition){
				
				if(temp.equals(prohibition.getAtc().getAtcCode())){
					containAvoidance.remove(i);
					i--;
					break;
				}
				
			}
		}

		for(int i=0; i< containAvoidance.size();i++){
			temp = containAvoidance.get(i).getAtc().getAtcCode();		
			for(MedicineDataType capable: containCapable){
				if(temp.equals(capable.getAtc().getAtcCode())){
					containAvoidance.remove(i);
					i--;
					break;
				}
				
			}
		}
		
		if(containAvoidance.size() == 0)
			containAvoidance = null;
		
		if(containProhibition.size() == 0)
			containProhibition = null;
		
		
		request.setAttribute("capableList", containCapable);
		request.setAttribute("avoidanceList", containAvoidance);
		request.setAttribute("prohibitionList", containProhibition);

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/select/patient_alergyResult.jsp");
		dispatcher.forward(request, response);
	}
	private void selectDoctorRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setAttribute("doctorId",request.getParameter("doctorId"));
		
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/select/patient_inputMedicines.jsp");
		dispatcher.forward(request, response);
				
	}
	
	private void MyAlergyDetailRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//환자 아이디로 자신을 등록한 의사의 아이디 검색
		HttpSession session = request.getSession();
		UserDataType userDataType = (UserDataType) session.getAttribute("user");
		
		List<RegistrationDataType> registrationList = this.prescriptionManager.selectRegiDoctorByPatientId(userDataType);
				
		request.setAttribute("myDoctorList", registrationList);
		
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/select/patient_myDoctorList.jsp");
		dispatcher.forward(request, response);
		
	}
	
	private void patientDetailAlergyRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		SearchParam param = new SearchParam();
		UserDataType user = (UserDataType) session.getAttribute("user");
		param.setId((String) session.getAttribute("patientId"));
		param.setParam(user.getUserId());

		param.setIdx(this.prescriptionManager.selectRegiByParam(param).getIdx());
		
		List<ProhibitionDataType> prohibitionList = this.prescriptionManager.selectProhibitionByIdx(param);
		List<UpperDataType> upperList = this.prescriptionManager.selectUpperAtcByIdx(param);
		List<CapableDataType> capableList = this.prescriptionManager.selectCapableByIdx(param);
		String registrationDate = this.prescriptionManager.selectRegiByParam(param).getRegistrationDate().toString();
		if((prohibitionList == null) || (upperList == null) || (capableList == null))
		{
			request.setAttribute("object", false);
		}
		else{
			request.setAttribute("object", true);
			
			request.setAttribute("prohibitionList", prohibitionList);
			request.setAttribute("upperList", upperList);
			request.setAttribute("capableList", capableList);
			request.setAttribute("registrationDate", registrationDate);
		}
		
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/select/doctor_patientDetailAlergy.jsp");
		dispatcher.forward(request, response);
		
	}
	
	private void finalCheckRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		
		JSONObject json = new JSONObject();
		response.setCharacterEncoding("UTF-8");
		
		SearchParam param = new SearchParam();
		UserDataType user = (UserDataType) session.getAttribute("user");
		param.setId((String) session.getAttribute("patientId"));
		
		
		List<UpperDataType> upperDataTypeList = new ArrayList<UpperDataType>();
		String[] upperList = request.getParameter("upperList").split(" ");
	
		for(String upper: upperList){
			UpperDataType upperDataType = new UpperDataType();
			
			if(!upper.equals("")){
				param.setParam(upper);				
				upperDataType.setAtc(this.prescriptionManager.atcCheck(param));
				
				param.setParam(user.getUserId());
				upperDataType.setRegistrationIdx(this.prescriptionManager.selectRegiByParam(param).getIdx());
				
				upperDataTypeList.add(upperDataType);
				
			}
			upperDataType = null;
		}
		
		List<ProhibitionDataType> prohibitionDataTypeList = new ArrayList<ProhibitionDataType>();
		String[] prohibitionList = request.getParameter("prohibitionList").split(" ");

		for(String prohibition: prohibitionList){
			ProhibitionDataType prohibitionDataType = new ProhibitionDataType();
			
			if(!prohibition.equals("")){
				param.setParam(prohibition);				
				prohibitionDataType.setProhibitedMedicine(this.prescriptionManager.selectMedicineByCode(param));
				
				param.setParam(user.getUserId());
				prohibitionDataType.setRegistrationIdx(this.prescriptionManager.selectRegiByParam(param).getIdx());
				
				
				prohibitionDataTypeList.add(prohibitionDataType);
				
			}
			prohibitionDataType = null;
		}
		
		List<CapableDataType> capableDataTypeList = new ArrayList<CapableDataType>();
		String[] capableList = request.getParameter("capableList").split(" ");

		for(String capable: capableList){
			CapableDataType capableDataType = new CapableDataType();
			
			if(!capable.equals("")){
				param.setParam(capable);				
				capableDataType.setCapableMedicine(this.prescriptionManager.selectMedicineByCode(param));
				
				param.setParam(user.getUserId());
				capableDataType.setRegistrationIdx(this.prescriptionManager.selectRegiByParam(param).getIdx());
				
				
				capableDataTypeList.add(capableDataType);
				
			}
			capableDataType = null;
		}
		
		Utils.errorCheck check = null;
		Utils.errorCheck check2 = null;
		// 금기약제랑 상위ATC 코드가 같은지 또는 금기약제랑 복용약제가 같은지
		
		for(ProhibitionDataType prohibition: prohibitionDataTypeList){
			
			for(UpperDataType upper: upperDataTypeList){
				
				if(upper.getAtc().getAtcCode().equals(prohibition.getProhibitedMedicine().getAtc().getAtcCode())){
					// 금지약제랑 상위ATC 코드와 같음
					
					check = Utils.errorCheck.PROHIBITION_SAME_UPPERATC;
				}
				
				if(!prohibition.getProhibitedMedicine().getAtc().getAtcCode().contains(upper.getAtc().getAtcCode())){
					check2 = Utils.errorCheck.UPPERATC_NOT_IN_PROHIBITION;
				}
				else
					check2 = null;
			}	
			
			if(check2 == Utils.errorCheck.UPPERATC_NOT_IN_PROHIBITION)
				break;
			
			for(CapableDataType capable: capableDataTypeList){
				
				if(capable.getCapableMedicine().getAtc().getAtcCode().equals(prohibition.getProhibitedMedicine().getAtc().getAtcCode())){
					// 복용가능약제랑 금지약제랑 같은지
					check = Utils.errorCheck.PROHIBITION_SAME_CAPABLE;
				}
				
			}
		}
		
		for(CapableDataType capable: capableDataTypeList){
			
			for(UpperDataType upper: upperDataTypeList){
			
				if(upper.getAtc().getAtcCode().equals(capable.getCapableMedicine().getAtc().getAtcCode())){
					
					check = Utils.errorCheck.CAPABLE_SAME_UPPERATC;
				}
				
				if(!capable.getCapableMedicine().getAtc().getAtcCode().contains(upper.getAtc().getAtcCode())){
					check2 = Utils.errorCheck.UPPERATC_NOT_IN_CAPABLE;
				}
				else
					check2 = null;
			}
			
			if(check2 == Utils.errorCheck.UPPERATC_NOT_IN_CAPABLE)
				break;
			
		}

		
		
		if( check == null && check2 == null){

			json.put("resultOk", true);
		}
		
		else{
			
			json.put("resultOk", false);
			
			if(check == Utils.errorCheck.PROHIBITION_SAME_UPPERATC){
				json.put("msg", "PROHIBITION_SAME_UPPERATC");
			}
			else if(check == Utils.errorCheck.PROHIBITION_SAME_CAPABLE){
				json.put("msg", "PROHIBITION_SAME_CAPABLE");
			}
			else if(check == Utils.errorCheck.CAPABLE_SAME_UPPERATC){
				json.put("msg", "CAPABLE_SAME_UPPERATC");
			}
			
			if(check2 == Utils.errorCheck.UPPERATC_NOT_IN_PROHIBITION){
				json.put("msg", "UPPERATC_NOT_IN_PROHIBITION");
			}
			else if(check2 == Utils.errorCheck.UPPERATC_NOT_IN_CAPABLE){
				json.put("msg", "UPPERATC_NOT_IN_CAPABLE");
			}
		}
			
		PrintWriter out = response.getWriter();
		out.print(json);
	}
	private void finalCancelRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		request.setAttribute("prohibitionList","");
		request.setAttribute("upperList", "");
		request.setAttribute("capableList", "");
		
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/select/doctor_selectedPatient.jsp");
		dispatcher.forward(request, response);
	}
	private void insertAlergyFinalRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		SearchParam param = new SearchParam();
		param.setId((String) session.getAttribute("patientId"));
		UserDataType user = (UserDataType) session.getAttribute("user");
		
		List<ProhibitionDataType> prohibitionDataTypeList = new ArrayList<ProhibitionDataType>();
		String val=null;
		int i=1;
		while((val = (String) request.getParameter("prohibition"+i)) != null){
			ProhibitionDataType prohibitionDataType = new ProhibitionDataType();
			
			param.setParam(val);				
			prohibitionDataType.setProhibitedMedicine(this.prescriptionManager.selectMedicineByCode(param));
			
			param.setParam(user.getUserId());
			prohibitionDataType.setRegistrationIdx(this.prescriptionManager.selectRegiByParam(param).getIdx());
			
			prohibitionDataTypeList.add(prohibitionDataType);
			
			prohibitionDataType = null;
			
			i++;
		}
		
		request.setAttribute("prohibitionList", prohibitionDataTypeList);
		
		List<UpperDataType> upperDataTypeList = new ArrayList<UpperDataType>();
		val=null;
		i=1;
		while((val = (String) request.getParameter("upper"+i)) != null){
			UpperDataType upperDataType = new UpperDataType();
			
			param.setParam(val);				
			upperDataType.setAtc(this.prescriptionManager.atcCheck(param));
			
			param.setParam(user.getUserId());
			upperDataType.setRegistrationIdx(this.prescriptionManager.selectRegiByParam(param).getIdx());
			
			upperDataTypeList.add(upperDataType);
			
			upperDataType = null;
			
			i++;
		}
		
		request.setAttribute("upperList", upperDataTypeList);
		
		List<CapableDataType> capableDataTypeList = new ArrayList<CapableDataType>();
		
		val=null;
		i=1;
		while((val = (String) request.getParameter("capable"+i)) != null){
			CapableDataType capableDataType = new CapableDataType();
			
			param.setParam(val);				
			capableDataType.setCapableMedicine(this.prescriptionManager.selectMedicineByCode(param));
			
			param.setParam(user.getUserId());
			capableDataType.setRegistrationIdx(this.prescriptionManager.selectRegiByParam(param).getIdx());
			
			capableDataTypeList.add(capableDataType);
			
			capableDataType = null;
			
			i++;
		}
		
		request.setAttribute("upperList", upperDataTypeList);
		
		param.setIdx(prohibitionDataTypeList.get(0).getRegistrationIdx());
		if(this.prescriptionManager.confirmProhibitionByIdx(param) != 0){
			this.prescriptionManager.deleteProhibition(param);
		}
		for(ProhibitionDataType prohibition : prohibitionDataTypeList){				
			this.prescriptionManager.insertProhibition(prohibition);
		}
		
		param.setIdx(upperDataTypeList.get(0).getRegistrationIdx());
		if(this.prescriptionManager.confirmUpperAtcByIdx(param) != 0){
			this.prescriptionManager.deleteUpperAtc(param);
		}
		for(UpperDataType upper : upperDataTypeList){
			this.prescriptionManager.insertUpperAtc(upper);
		}
	
		param.setIdx(capableDataTypeList.get(0).getRegistrationIdx());
		if(this.prescriptionManager.confirmCapableByIdx(param) != 0){
			this.prescriptionManager.deleteCapable(param);
		}
		for(CapableDataType capable : capableDataTypeList){	
			this.prescriptionManager.insertCapable(capable);
		}
		

		List<RegistrationDataType> patients = this.registrationController.detailPatientList(user);
		request.setAttribute("patientList", patients);
		
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/select/doctor_patientList.jsp");
		dispatcher.forward(request, response);
	}
	private void capableCheckRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		
		JSONObject json = new JSONObject();
		response.setCharacterEncoding("UTF-8");

		SearchParam param = new SearchParam();
		param.setParam(request.getParameter("capable"));
		MedicineDataType m = this.prescriptionManager.medicineCheck(param);
		UserDataType user = (UserDataType) session.getAttribute("user");
		param.setId((String) session.getAttribute("patientId"));
		
		if( m == null){

			json.put("isExist", false);
			json.put("medicine", null);
		}
		
		else{
			
			json.put("isExist", true);
			json.put("medicineCode",m.getCode());
			json.put("medicinefullName",m.getFullName());
			json.put("medicineATCcode",m.getAtc().getAtcCode());
			json.put("medicineATCname",m.getAtc().getAtcName());
		
		}
		
		if( m != null){
			
			List<UpperDataType> upperDataTypeList = new ArrayList<UpperDataType>();
			String[] upperList = request.getParameter("upperList").split(" ");
		
			for(String upper: upperList){
				UpperDataType upperDataType = new UpperDataType();
				
				if(!upper.equals("")){
					param.setParam(upper);				
					upperDataType.setAtc(this.prescriptionManager.atcCheck(param));
					
					param.setParam(user.getUserId());
					upperDataType.setRegistrationIdx(this.prescriptionManager.selectRegiByParam(param).getIdx());
					
					upperDataTypeList.add(upperDataType);
					
				}
				upperDataType = null;
			}
			
			List<ProhibitionDataType> prohibitionDataTypeList = new ArrayList<ProhibitionDataType>();
			String[] prohibitionList = request.getParameter("prohibitionList").split(" ");

			for(String prohibition: prohibitionList){
				ProhibitionDataType prohibitionDataType = new ProhibitionDataType();
				
				if(!prohibition.equals("")){
					param.setParam(prohibition);				
					prohibitionDataType.setProhibitedMedicine(this.prescriptionManager.selectMedicineByCode(param));
					
					param.setParam(user.getUserId());
					prohibitionDataType.setRegistrationIdx(this.prescriptionManager.selectRegiByParam(param).getIdx());
					
					
					prohibitionDataTypeList.add(prohibitionDataType);
					
				}
				prohibitionDataType = null;
			}
			
			json.put("msg", null);
		
			for(UpperDataType upper: upperDataTypeList){
			
				if(!m.getAtc().getAtcCode().contains(upper.getAtc().getAtcCode()))
					json.put("msg", "회피상위 ATC코드에 해당되는 복용가능 약제가 아닙니다.");
				else if(m.getAtc().getAtcCode().equals(upper.getAtc().getAtcCode()))
					json.put("msg", "입력하신 해당 복용약제의 ATC코드가 회피상위 ATC의 코드와 같습니다.");
				else
					json.put("msg", null);
				
				if(json.get("msg") == null)
					break;
			}
			
			for(ProhibitionDataType prohibition: prohibitionDataTypeList){
				if(prohibition.getProhibitedMedicine().getAtc().getAtcCode().equals(m.getAtc().getAtcCode())){
					json.put("msg", "입력하신 해당 복용약제의 ATC코드가 금지약제 ATC 코드와 같습니다.");
					break;
				}
			}
		}

		PrintWriter out = response.getWriter();
		out.print(json);
	}
	private void insertAlergySecondRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		String[] values = request.getParameter("atcList").split(" ");
		SearchParam param = new SearchParam();
		param.setId((String) session.getAttribute("patientId"));
		UserDataType user = (UserDataType) session.getAttribute("user");
		
		List<ProhibitionDataType> prohibitionDataTypeList = new ArrayList<ProhibitionDataType>();
		String val=null;
		int i=1;
		while((val = (String) request.getParameter("prohibition"+i)) != null){
			ProhibitionDataType prohibitionDataType = new ProhibitionDataType();
			
			param.setParam(val);				
			prohibitionDataType.setProhibitedMedicine(this.prescriptionManager.selectMedicineByCode(param));
			
			param.setParam(user.getUserId());
			prohibitionDataType.setRegistrationIdx(this.prescriptionManager.selectRegiByParam(param).getIdx());
			
			prohibitionDataTypeList.add(prohibitionDataType);
			
			prohibitionDataType = null;
			
			i++;
		}
		
		request.setAttribute("prohibitionList", prohibitionDataTypeList);

		List<UpperDataType> upperList = new ArrayList<UpperDataType>();
		
		for(String value: values){
			UpperDataType upperDataType = new UpperDataType();
			
			if(!value.equals("")){
				param.setParam(value);
				upperDataType.setAtc(this.prescriptionManager.atcCheck(param));
				
				param.setParam(user.getUserId());
				upperDataType.setRegistrationIdx(this.prescriptionManager.selectRegiByParam(param).getIdx());
				
				upperList.add(upperDataType);
				
			}
			upperDataType = null;
		}

		request.setAttribute("upperList", upperList);
		
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/select/doctor_selectedUpperAtc.jsp");
		dispatcher.forward(request, response);
		
	}
	private void upperATCCheckRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		HttpSession session = request.getSession();
		
		JSONObject json = new JSONObject();
		response.setCharacterEncoding("UTF-8");
		
		SearchParam param = new SearchParam();
		param.setParam(request.getParameter("upperATC"));
		param.setId((String) session.getAttribute("patientId"));
		UserDataType user = (UserDataType) session.getAttribute("user");
	
		
		AtcDataType m = this.prescriptionManager.atcCheck(param);
			
		if( m == null){

			json.put("isExist", false);
			json.put("atc", null);
		}
		
		else{
			
			json.put("isExist", true);
			json.put("atcCode",m.getAtcCode());
			json.put("atcName",m.getAtcName());
		
		}
			
		if( m != null){
			//nam
			List<ProhibitionDataType> prohibitionDataTypeList = new ArrayList<ProhibitionDataType>();
			String[] prohibitionList = request.getParameter("prohibitionList").split(" ");

			for(String prohibition: prohibitionList){
				ProhibitionDataType prohibitionDataType = new ProhibitionDataType();
				
				if(!prohibition.equals("")){
					param.setParam(prohibition);				
					prohibitionDataType.setProhibitedMedicine(this.prescriptionManager.selectMedicineByCode(param));
					
					param.setParam(user.getUserId());
					prohibitionDataType.setRegistrationIdx(this.prescriptionManager.selectRegiByParam(param).getIdx());
					
					
					prohibitionDataTypeList.add(prohibitionDataType);
					
				}
				prohibitionDataType = null;
			}
			
			json.put("msg", null);
			
			
			
			for(ProhibitionDataType prohibition: prohibitionDataTypeList){
			
				if(prohibition.getProhibitedMedicine().getAtc().getAtcCode().equals(m.getAtcCode()) ||
						!prohibition.getProhibitedMedicine().getAtc().getAtcCode().contains(m.getAtcCode()))
					json.put("msg", "금기약제의 ATC의 코드와 같거나 해당 상위 ATC코드가 아닙니다.");
				else
					json.put("msg", null);
				
				if(json.get("msg") == null)
					break;
			}	
		}

		PrintWriter out = response.getWriter();
		out.print(json);
	}
	private void prohibitionCheckRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		
		JSONObject json = new JSONObject();
		response.setCharacterEncoding("UTF-8");
		
		SearchParam param = new SearchParam();
		param.setParam(request.getParameter("prohibition"));
		MedicineDataType m = this.prescriptionManager.medicineCheck(param);
		
		if( m == null){

			json.put("isExist", false);
			json.put("medicine", null);
		}
		
		else{
			
			json.put("isExist", true);
			json.put("medicineCode",m.getCode());
			json.put("medicinefullName",m.getFullName());
			json.put("medicineATCcode",m.getAtc().getAtcCode());
			json.put("medicineATCname",m.getAtc().getAtcName());
		
		}

		PrintWriter out = response.getWriter();
		out.print(json);
	}
	private void insertAlergyThirdRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		HttpSession session = request.getSession();
		String[] values = request.getParameter("medicineList").split(" ");
		SearchParam param = new SearchParam();
		param.setId((String) session.getAttribute("patientId"));
		UserDataType user = (UserDataType) session.getAttribute("user");
		
		//nam3
		List<ProhibitionDataType> prohibitionDataTypeList = new ArrayList<ProhibitionDataType>();
		String val=null;
		int i=1;
		while((val = (String) request.getParameter("prohibition"+i)) != null){
			ProhibitionDataType prohibitionDataType = new ProhibitionDataType();
			
			param.setParam(val);				
			prohibitionDataType.setProhibitedMedicine(this.prescriptionManager.selectMedicineByCode(param));
			
			param.setParam(user.getUserId());
			prohibitionDataType.setRegistrationIdx(this.prescriptionManager.selectRegiByParam(param).getIdx());
			
			prohibitionDataTypeList.add(prohibitionDataType);
			
			prohibitionDataType = null;
			
			i++;
		}
		
		request.setAttribute("prohibitionList", prohibitionDataTypeList);
		
		List<UpperDataType> upperDataTypeList = new ArrayList<UpperDataType>();
		String val2=null;
		int j=1;
		while((val2 = (String) request.getParameter("upper"+j)) != null){
			UpperDataType UpperDataType = new UpperDataType();
			
			param.setParam(val2);				
			UpperDataType.setAtc(this.prescriptionManager.atcCheck(param));
			
			param.setParam(user.getUserId());
			UpperDataType.setRegistrationIdx(this.prescriptionManager.selectRegiByParam(param).getIdx());
			
			upperDataTypeList.add(UpperDataType);
			
			UpperDataType = null;
			
			j++;
		}
		
		request.setAttribute("upperList", upperDataTypeList);
		
		List<CapableDataType> capableList = new ArrayList<CapableDataType>();
		
		for(String value: values){
			CapableDataType capableDataType = new CapableDataType();
			
			if(!value.equals("")){
				param.setParam(value);
				capableDataType.setCapableMedicine(this.prescriptionManager.selectMedicineByCode(param));
				
				param.setParam(user.getUserId());
				capableDataType.setRegistrationIdx(this.prescriptionManager.selectRegiByParam(param).getIdx());
				
				capableList.add(capableDataType);
				
				
			}
			capableDataType = null;
		}

		request.setAttribute("capableList", capableList);		

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/insert/doctor_selectedCapable.jsp");
		dispatcher.forward(request, response);
		
	}
	private void insertAlergyFirstRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		String[] values = request.getParameter("medicineList").split(" ");
		SearchParam param = new SearchParam();
		param.setId((String) session.getAttribute("patientId"));
		UserDataType user = (UserDataType) session.getAttribute("user");
		
		List<ProhibitionDataType> prohibitionList = new ArrayList<ProhibitionDataType>();

	
		for(String value: values){
			ProhibitionDataType prohibitionDataType = new ProhibitionDataType();
			
			if(!value.equals("")){
				
				param.setParam(value);
				prohibitionDataType.setProhibitedMedicine(this.prescriptionManager.selectMedicineByCode(param));
				
				param.setParam(user.getUserId());
				prohibitionDataType.setRegistrationIdx(this.prescriptionManager.selectRegiByParam(param).getIdx());
				
				
				prohibitionList.add(prohibitionDataType);	
			}
			prohibitionDataType=null;
		}
	
		request.setAttribute("prohibitionList", prohibitionList);
		
		
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/select/doctor_selectedProhibition.jsp");
		dispatcher.forward(request, response);
		
	}
	private void selectPatientRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// 입력받은 환자의 id를 이용하여 
		HttpSession session = request.getSession();
		session.setAttribute("patientId",request.getParameter("patientId"));
		
		
		this.registrationController.getRegistrationDataRequest(request, response);

		RequestDispatcher dispatcher = request.getRequestDispatcher("/prescription/patientDetailAlergy");
		dispatcher.forward(request, response);		

	}
	private void logoutCheckRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		
		session.setAttribute("user", null);
		
		RequestDispatcher dispatcher = request.getRequestDispatcher("/index.jsp");
		dispatcher.forward(request, response);
	}
}