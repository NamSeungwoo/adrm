package kr.ac.cbnu.computerengineering.user.manager.dao;

import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;

import kr.ac.cbnu.computerengineering.common.datatype.ApprovalDataType;
import kr.ac.cbnu.computerengineering.common.datatype.SearchParam;
import kr.ac.cbnu.computerengineering.common.datatype.UserDataType;
import kr.ac.cbnu.computerengineering.common.datatype.UserRoleDataType;
import kr.ac.cbnu.computerengineering.common.managers.dao.IUserDao;
import kr.ac.cbnu.computerengineering.common.mybatis.Mybatis;

public class UserDaoImpl implements IUserDao {
	private SqlSessionFactory sqlSessionFactory;
	
	public UserDaoImpl() {
		// TODO Auto-generated constructor stub
		this.sqlSessionFactory = Mybatis.getSqlSessionFactory();
	}
	
	@Override
	public UserDataType detailUser(UserDataType userDataType) {
		// TODO Auto-generated method stub
		SqlSession session = this.sqlSessionFactory.openSession();
		UserDataType result = null;
		try {
			result = session.selectOne("user.detailUser",userDataType);
			
		} finally {
			session.close();
		}
		
		return result;
	}
	public int idCheck(UserDataType userDataType) {
		// TODO Auto-generated method stub
		SqlSession session = this.sqlSessionFactory.openSession();
		int result = 0;
		try {
			
			result = session.selectOne("user.idCheck",userDataType);
			
		} finally {
			session.close();
		}
		
		return result;
	}
	
	public int insert(UserDataType userDataType){
		SqlSession session = this.sqlSessionFactory.openSession(true);
		int result = 0;
		try {
			result = session.insert("user.insertUser", userDataType);
		} finally {
			session.close();
		}
		
		return result;
	}
	
	public void updateUserInform(UserDataType userDataType){
		SqlSession session = this.sqlSessionFactory.openSession(true);
		try {
			session.update("user.updateUserInform", userDataType);
		} finally {
			session.close();
		}
		
	}

	public void disableAccount(UserDataType userDataType){
		SqlSession session = this.sqlSessionFactory.openSession(true);
		try {
			session.update("user.disableAccount", userDataType);
		} finally {
			session.close();
		}
	}
	
	public List<UserDataType> selectAccounts(SearchParam param){
		SqlSession session = this.sqlSessionFactory.openSession();
		List<UserDataType> result = null;
		try {
			result = session.selectList("user.selectAccounts",param);
			
		} finally {
			session.close();
		}
		
		return result;
	}
	public int selectAllUserCounts(){
		SqlSession session = this.sqlSessionFactory.openSession();
		int result = 0;
		try {
			
			result = session.selectOne("user.selectAllUserCounts");

		} finally {
			session.close();
		}
		return result;
	}
	public void deleteUser(UserDataType userDataType){
		SqlSession session = this.sqlSessionFactory.openSession(true);
		try {
			
			session.delete("user.deleteUser",userDataType);

		} finally {
			session.close();
		}
	}
	public void deleteUserRoleInform(UserDataType userDataType){
		SqlSession session = this.sqlSessionFactory.openSession(true);
		try {
			
			session.delete("user.deleteUserRoleInform",userDataType);

		} finally {
			session.close();
		}
	}
	public List<UserDataType> selectAccountById(SearchParam param){
		SqlSession session = this.sqlSessionFactory.openSession();
		List<UserDataType> result = null;
		try {
			result = session.selectList("user.selectAccountById",param);
			
		} finally {
			session.close();
		}
		
		return result;
	}
	public List<UserDataType> selectAccountByName(SearchParam param){
		SqlSession session = this.sqlSessionFactory.openSession();
		List<UserDataType> result = null;
		try {
			result = session.selectList("user.selectAccountByName",param);
			
		} finally {
			session.close();
		}
		
		return result;
	}
	public List<ApprovalDataType> selectApprovals(SearchParam param){
		SqlSession session = this.sqlSessionFactory.openSession();
		List<ApprovalDataType> result = null;
		try {
			result = session.selectList("user.selectApprovals",param);
			
		} finally {
			session.close();
		}
		
		return result;	
	}
	public int selectAllApprovalCounts(){
		SqlSession session = this.sqlSessionFactory.openSession();
		int result = 0;
		try {
			
			result = session.selectOne("user.selectAllApprovalCounts");

		} finally {
			session.close();
		}
		return result;
	}
	public List<ApprovalDataType> selectApprovalById(ApprovalDataType approvalDataType){
		SqlSession session = this.sqlSessionFactory.openSession();
		List<ApprovalDataType> result = null;
		try {
			
			result = session.selectList("user.selectApprovalById", approvalDataType);

		} finally {
			session.close();
		}
		return result;
	}
	public void updateApprovalById(ApprovalDataType approvalDataType){
		SqlSession session = this.sqlSessionFactory.openSession(true);
		try {
			
			session.update("user.updateApprovalById", approvalDataType);

		} finally {
			session.close();
		}

	}
	public void deleteApproval(ApprovalDataType approvalDataType){
		SqlSession session = this.sqlSessionFactory.openSession(true);
		try {
			
			session.delete("user.deleteApproval",approvalDataType);

		} finally {
			session.close();
		}
	}
	public int cbnuCodeCheck(UserDataType userDataType){
		SqlSession session = this.sqlSessionFactory.openSession();
		int result = 0;
		try {
			
			result = session.selectOne("user.cbnuCodeCheck",userDataType);
		} finally {
			session.close();
		}
		return result;
	}
	public void insertRoleAndUpdateApproval(UserRoleDataType userRoleDataType, ApprovalDataType approvalDataType){
		SqlSession session = this.sqlSessionFactory.openSession(true);
		try {
			session.insert("user.insertRoles", userRoleDataType);
			session.update("user.updateApprovalById", approvalDataType);
		} finally {
			session.close();
		}
	}
	public void deleteRoleAndUpdateApproval(UserRoleDataType userRoleDataType, ApprovalDataType approvalDataType){
		SqlSession session = this.sqlSessionFactory.openSession(true);
		try {
			session.delete("user.deleteRole", userRoleDataType);
			session.update("user.updateApprovalById", approvalDataType);
		} finally {
			session.close();
		}
	}
	public void deleteApprovalAndInsertRole(UserRoleDataType userRoleDataType, ApprovalDataType approvalDataType){
		SqlSession session = this.sqlSessionFactory.openSession(true);
		try {
			session.delete("user.deleteApproval", approvalDataType);
			session.insert("user.insertRoles", userRoleDataType);
		} finally {
			session.close();
		}
	}
	public UserDataType selectIdByCbnuCode(UserDataType userDataType){
		SqlSession session = this.sqlSessionFactory.openSession();
		UserDataType result = null;
		try {
			
			result = session.selectOne("user.selectIdByCbnuCode", userDataType);

		} finally {
			session.close();
		}
		return result;
	}
	public UserDataType selectMailByIdAndCode(UserDataType userDataType){
		SqlSession session = this.sqlSessionFactory.openSession();
		UserDataType result = null;
		try {
			
			result = session.selectOne("user.selectMailByIdAndCode", userDataType);

		} finally {
			session.close();
		}
		return result;
	}
	public void changePassword(UserDataType userDataType){
		SqlSession session = this.sqlSessionFactory.openSession(true);
		try {
			session.update("user.changePassword", userDataType);
		} finally {
			session.close();
		}
	}
}
