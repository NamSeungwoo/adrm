package kr.ac.cbnu.computerengineering.user.manager;

import java.util.List;

import kr.ac.cbnu.computerengineering.common.datatype.ApprovalDataType;
import kr.ac.cbnu.computerengineering.common.datatype.SearchParam;
import kr.ac.cbnu.computerengineering.common.datatype.UserDataType;
import kr.ac.cbnu.computerengineering.common.datatype.UserRoleDataType;
import kr.ac.cbnu.computerengineering.common.datatype.UserRoleType;
import kr.ac.cbnu.computerengineering.common.managers.IUserManager;
import kr.ac.cbnu.computerengineering.common.managers.dao.IRoleDao;
import kr.ac.cbnu.computerengineering.common.managers.dao.IUserDao;
import kr.ac.cbnu.computerengineering.user.manager.dao.RoleDaoImpl;
import kr.ac.cbnu.computerengineering.user.manager.dao.UserDaoImpl;

public class UserManagerImpl implements IUserManager {
	private IUserDao userDao;
	private IRoleDao roleDao;
	
	public UserManagerImpl() {
		// TODO Auto-generated constructor stub
		this.userDao = new UserDaoImpl();
		this.roleDao = new RoleDaoImpl();
	}
	@Override
	public UserDataType getUser(UserDataType userDataType) {
		// TODO Auto-generated method stub
		return this.userDao.detailUser(userDataType);
	}
	
	public void insertUser(UserDataType userDataType){
		this.userDao.insert(userDataType);
		for(UserRoleDataType type : userDataType.getRoles()) {
			if(type.getUserRole().equals(UserRoleType.DOCTOR))
				this.roleDao.insertApproval(type);
			else
				this.roleDao.insert(type);
		}	
		
	}
	public UserDataType checkLogin(UserDataType userDataType){
		return this.userDao.detailUser(userDataType);
	}
	public List<UserRoleDataType> selectRoles(UserDataType userDataType){
		return this.roleDao.detailsRoles(userDataType);
	}
	public int idCheck(UserDataType userDataType){
		return this.userDao.idCheck(userDataType);
	}
	public void changeUserInform(UserDataType userDataType, boolean isDoctor){
		// TODO 의사이고 수정한게 의사이면 지우지 않기
		this.roleDao.deleteUserRoleInform(userDataType);
		this.userDao.updateUserInform(userDataType);
		ApprovalDataType approvalDataType = new ApprovalDataType();
		approvalDataType.setUserId(userDataType.getUserId());
		
		boolean doctorSameFlag = false;
		for(UserRoleDataType type : userDataType.getRoles()) {
			if(isDoctor == true)
			{
				if(type.getUserRole().equals(UserRoleType.DOCTOR)){
					doctorSameFlag = true;
				}
				this.roleDao.insert(type);

			}
			else{
				if(type.getUserRole().equals(UserRoleType.DOCTOR)){
					this.roleDao.insertApproval(type);
				}
				else{
					this.roleDao.insert(type);
				}
			}
		}
		
		if(isDoctor == true && doctorSameFlag == false){
			this.userDao.deleteApproval(approvalDataType);
		}
	}
	public void disableAccount(UserDataType userDataType){
		this.userDao.disableAccount(userDataType);
	}
	public List<UserDataType> selectAccounts(SearchParam param){
		return this.userDao.selectAccounts(param);
	}
	public int selectAllUserCounts(){
		return this.userDao.selectAllUserCounts();
	}
	public void deleteUser(UserDataType userDataType){
		this.userDao.deleteUser(userDataType);
	}
	public void deleteUserRoleInform(UserDataType userDataType){
		this.userDao.deleteUserRoleInform(userDataType);
	}
	public List<UserDataType> selectAccountById(SearchParam param){
		return this.userDao.selectAccountById(param);
	}
	public List<UserDataType> selectAccountByName(SearchParam param){
		return this.userDao.selectAccountByName(param);
	}
	public List<ApprovalDataType> selectApprovals(SearchParam param){
		return this.userDao.selectApprovals(param);
	}
	public int selectAllApprovalCounts(){
		return this.userDao.selectAllApprovalCounts();
	}
	public List<ApprovalDataType> selectApprovalById(ApprovalDataType approvalDataType){
		return this.userDao.selectApprovalById(approvalDataType);
	}
	public void updateApprovalById(ApprovalDataType approvalDataType){
		this.userDao.updateApprovalById(approvalDataType);
	}
	public void insertRole(UserRoleDataType userRoleDataType){
		this.roleDao.insert(userRoleDataType);
	}
	public void deleteRole(UserRoleDataType userRoleDataType){
		this.roleDao.deleteRole(userRoleDataType);
	}
	public List<UserRoleDataType> selectRole(UserRoleDataType userRoleDataType){
		return this.roleDao.selectRole(userRoleDataType);
	}
	public void deleteApproval(ApprovalDataType approvalDataType){
		this.userDao.deleteApproval(approvalDataType);
	}
	public int cbnuCodeCheck(UserDataType userDataType){
		return this.userDao.cbnuCodeCheck(userDataType);
	}
	public void insertRoleAndUpdateApproval(UserRoleDataType userRoleDataType, ApprovalDataType approvalDataType){
		this.userDao.insertRoleAndUpdateApproval(userRoleDataType, approvalDataType);
	}
	public void deleteRoleAndUpdateApproval(UserRoleDataType userRoleDataType, ApprovalDataType approvalDataType){
		this.userDao.deleteRoleAndUpdateApproval(userRoleDataType, approvalDataType);
	}
	public UserDataType selectIdByCbnuCode(UserDataType userDataType){
		return this.userDao.selectIdByCbnuCode(userDataType);
	}
	public UserDataType selectMailByIdAndCode(UserDataType userDataType){
		return this.userDao.selectMailByIdAndCode(userDataType);
	}
	public void changePassword(UserDataType userDataType){
		this.userDao.changePassword(userDataType);
	}
}
