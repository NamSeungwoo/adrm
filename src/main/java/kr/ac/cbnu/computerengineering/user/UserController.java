package kr.ac.cbnu.computerengineering.user;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.json.simple.JSONObject;

import kr.ac.cbnu.computerengineering.common.datatype.ApprovalDataType;
import kr.ac.cbnu.computerengineering.common.datatype.ApprovalType;
import kr.ac.cbnu.computerengineering.common.datatype.PagingDataType;
import kr.ac.cbnu.computerengineering.common.datatype.SearchParam;
import kr.ac.cbnu.computerengineering.common.datatype.UserDataType;
import kr.ac.cbnu.computerengineering.common.datatype.UserRoleDataType;
import kr.ac.cbnu.computerengineering.common.datatype.UserRoleType;
import kr.ac.cbnu.computerengineering.common.managers.IUserManager;
import kr.ac.cbnu.computerengineering.common.util.Utils;
import kr.ac.cbnu.computerengineering.prescription.RegistrationController;
import kr.ac.cbnu.computerengineering.user.manager.UserManagerImpl;

/**
 * Servlet implementation class UserController
 */
@WebServlet("/UserController")
public class UserController extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	private IUserManager userManager;
    private RegistrationController registrationController;
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserController() {
        super();
        this.userManager = new UserManagerImpl(); //or UserManagerImpl2();
        this.registrationController = new RegistrationController();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//response.getWriter().append("Served at: ").append(request.getContextPath());
		try {
			checkURL(request, response);
		} catch (MessagingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		try {
			checkURL(request, response);
		} catch (MessagingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	private void checkURL(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, AddressException, MessagingException {
		
		String url = request.getRequestURI();
		url = url.replace("/ADRM/user/", "");
		if(url.equals("membership_end")){
			this.membershipEndRequest(request, response);
		}
		else if(url.equals("userManagement")){
			this.userManagementRequest(request, response);
		}
		else if(url.equals("accountManagementSearch")){
			this.accountManagementSearchRequest(request,response);
		}
		else if(url.equals("accountManagementFinal")){
			this.accountManagementFinalRequest(request,response);
		}
		else if(url.equals("accountManagement")){
			this.accountManagementRequest(request,response);
		}
		else if(url.equals("approvalManagement")){
			this.approvalManagementRequest(request,response);
		}
		else if(url.equals("approvalManagementFinal")){
			this.approvalManagementFinalRequest(request,response);
		}
		else if(url.equals("approvalManagementSearch")){
			this.approvalManagementSearchRequest(request,response);
		}
		else if(url.equals("modifyMember_end")){
			this.modifyMemberEndRequest(request,response);
		}
		else if(url.equals("login_end")){
			this.loginCheckRequest(request, response);
		}
		else if (url.contains("membership")){
			this.membershipRequest(request, response);
		}
		else if (url.contains("findId")){
			this.findIdRequest(request, response);
		}
		else if (url.contains("requestCheckId")){
			this.requestCheckIdRequest(request, response);
		}
		else if (url.contains("requestMail")){
			this.requestMailRequest(request, response);
		}
		else if (url.contains("findPassword")){
			this.findPasswordRequest(request, response);
		}
		else if (url.contains("disableAccount")){
			this.disableAccountRequest(request, response);
		}
		else if(url.contains("modifyMember")){
			this.modifyMemberRequest(request,response);
		}
		else if(url.contains("id_check")){
			this.idCheckRequest(request, response);
		}
		else if(url.contains("cbnuCode_check")){
			this.cbnuCodeCheckRequest(request, response);
		}
		else if(url.contains("login")){
			this.loginRequest(request, response);
		}
		else if(url.contains("main")){
			this.mainRequest(request, response);
		}
		else if(url.contains("logout")){
			this.logoutRequest(request, response);
		}
		else if(url.contains("mailSender")){
			this.mailSenderRequest(request, response);
		}
	}
	private void mailSenderRequest(HttpServletRequest request, HttpServletResponse response) throws AddressException, MessagingException, ServletException, IOException {
		
		UserDataType user = new UserDataType();
		user.setUserId(request.getParameter("_user"));
		user.setCbnuCode(request.getParameter("_cbnuCode"));
		user.setEmail(request.getParameter("_email"));
		
		String random = Utils.base64(user.getUserId() + user.getCbnuCode());
		
		user.setPassword(Utils.encryptSHA256(random));
		
		this.userManager.changePassword(user);
		
		// 네이버일 경우 smtp.naver.com 을 입력합니다.
		// Google일 경우 smtp.gmail.com 을 입력합니다.
		String host = "smtp.naver.com";
		
		final String username = "ubigamelab";       //네이버 아이디를 입력해주세요. @nave.com은 입력하지 마시구요.
		final String password = "E8-1423";   //네이버 이메일 비밀번호를 입력해주세요.
		int port=465; //포트번호
		 
		// 메일 내용
		String recipient = user.getEmail();    //받는 사람의 메일주소를 입력해주세요.
		String subject = "안녕하세요."+user.getUserId()+"님 충북대 알레르기 내과 입니다."; //메일 제목 입력해주세요.
		String body = "아래와 같이 임의의 비밀번호를 보내드리니 로그인 후 비밀번호를 바꿔주세요.\n\n"+random+"\n"; //메일 내용 입력해주세요.
		
		Properties props = System.getProperties(); // 정보를 담기 위한 객체 생성
		 
		// SMTP 서버 정보 설정
		props.put("mail.smtp.host", host);
		props.put("mail.smtp.port", port);
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.ssl.enable", "true");
		props.put("mail.smtp.ssl.trust", host);
		   
		//Session 생성
		Session session = Session.getDefaultInstance(props, new javax.mail.Authenticator() {
			String un=username;
			String pw=password;
			protected javax.mail.PasswordAuthentication getPasswordAuthentication() {
				return new javax.mail.PasswordAuthentication(un, pw);
			}
		});
		session.setDebug(true); //for debug
		   
		Message mimeMessage = new MimeMessage(session); //MimeMessage 생성
		mimeMessage.setFrom(new InternetAddress("ubigamelab@naver.com")); //발신자 셋팅 , 보내는 사람의 이메일주소를 한번 더 입력합니다. 이때는 이메일 풀 주소를 다 작성해주세요.
		mimeMessage.setRecipient(Message.RecipientType.TO, new InternetAddress(recipient)); //수신자셋팅 //.TO 외에 .CC(참조) .BCC(숨은참조) 도 있음


		mimeMessage.setSubject(subject);  //제목셋팅
		mimeMessage.setText(body);        //내용셋팅
		Transport.send(mimeMessage); //javax.mail.Transport.send() 이용
		
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/login/login.jsp");
		dispatcher.forward(request, response);
		
	}

	private void requestCheckIdRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		UserDataType userDataType = new UserDataType();
		userDataType.setCbnuCode(request.getParameter("cbnuCode"));
		JSONObject json = new JSONObject();
		UserDataType temp = null;
		
		if((temp = this.userManager.selectIdByCbnuCode(userDataType)) != null){

			json.put("isSuccess", true);
			json.put("msg",temp.getUserId().toString());
		}
		else{

			json.put("isSuccess", false);
		}
		

		PrintWriter out = response.getWriter();
		out.print(json);
	}
	private void requestMailRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		UserDataType userDataType = new UserDataType();
		userDataType.setUserId(request.getParameter("userId"));
		userDataType.setCbnuCode(request.getParameter("cbnuCode"));
		JSONObject json = new JSONObject();
		
		UserDataType temp = null;
		
		if((temp = this.userManager.selectMailByIdAndCode(userDataType)) != null){

			json.put("isSuccess", true);
			json.put("_user", userDataType.getUserId());
			json.put("_cbnuCode", userDataType.getCbnuCode());
			json.put("_email", temp.getEmail());
		}
		else{

			json.put("isSuccess", false);
		}

		PrintWriter out = response.getWriter();
		out.print(json);
	}
	private void findIdRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/login/find_id.jsp");
		dispatcher.forward(request, response);
	}
	private void findPasswordRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/login/find_password.jsp");
		dispatcher.forward(request, response);
	}
	private void approvalManagementSearchRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		ApprovalDataType approvalDataType = new ApprovalDataType();
		approvalDataType.setUserId(request.getParameter("search"));
		List<ApprovalDataType> approvalList = null;
		if(request.getParameter("searchKind").equals("id")){
			approvalList = this.userManager.selectApprovalById(approvalDataType);
		}
		
		request.setAttribute("approvalList", approvalList);
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/select/admin_searchApproval.jsp");
		dispatcher.forward(request, response);
	}
	private void approvalManagementFinalRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		int start = Integer.parseInt(request.getParameter("start"));
		int end = Integer.parseInt(request.getParameter("cnt"));
		
		ApprovalDataType approvalDataType = new ApprovalDataType();
		for(int i=start; i<=end; i++){
			approvalDataType.setUserId(request.getParameter("userId"+i));
			String data=null;
			if((data = request.getParameter("disable"+i)) != null){
				//db에 있는 값 최신
				ApprovalType approvalType = ApprovalType.valueOf(data);
				approvalDataType.setApproval(approvalType);
				UserRoleDataType userRoleDataType = new UserRoleDataType();
				userRoleDataType.setUserId(approvalDataType.getUserId());
				userRoleDataType.setUserRole(UserRoleType.DOCTOR);
				List<ApprovalDataType> temp = this.userManager.selectApprovalById(approvalDataType);
				if(approvalType.equals(ApprovalType.APPROVAL)){
					if(temp.get(0).getApproval().equals(ApprovalType.APPROVAL)){}
					else{
						this.userManager.insertRoleAndUpdateApproval(userRoleDataType, approvalDataType);
					}
				}
				else if(approvalType.equals(ApprovalType.REJECTION)){
					if(temp.get(0).getApproval().equals(ApprovalType.REJECTION)){}
					else{
						if(this.userManager.selectRole(userRoleDataType).isEmpty()){
							this.userManager.updateApprovalById(approvalDataType);
						}
						else{
							this.userManager.deleteRoleAndUpdateApproval(userRoleDataType,approvalDataType);
						}
						
					}		
				}
				else{
					if(temp.get(0).getApproval().equals(ApprovalType.WAITING)){}
					else{
						if(this.userManager.selectRole(userRoleDataType).isEmpty()){
							this.userManager.deleteRoleAndUpdateApproval(userRoleDataType,approvalDataType);
						}
						else {
							this.userManager.updateApprovalById(approvalDataType);
						}		
					}
				}
				
			}
			
		}
		
		RequestDispatcher dispatcher = request.getRequestDispatcher("/user/approvalManagement");
		dispatcher.forward(request, response);
	}
	private void accountManagementSearchRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		SearchParam param = new SearchParam();
		param.setParam(request.getParameter("search"));

		List<UserDataType> userList;
		if(request.getParameter("searchKind").equals("id")){
			userList = this.userManager.selectAccountById(param);
		}
		else{
			userList = this.userManager.selectAccountByName(param);
		}
		
		request.setAttribute("userList", userList);
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/select/admin_searchUser.jsp");
		dispatcher.forward(request, response);
	}
	
	private void accountManagementFinalRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		int start = Integer.parseInt(request.getParameter("start"));
		int end = Integer.parseInt(request.getParameter("cnt"));
		
		UserDataType userDataType = new UserDataType();
		for(int i=start; i<=end; i++){
			userDataType.setUserId(request.getParameter("userId"+i));

			if(request.getParameter("delete"+i) != null){
				this.userManager.deleteUserRoleInform(userDataType);
				this.userManager.deleteUser(userDataType);
			}
			else{
				userDataType.setDisable(request.getParameter("disable"+i));
				this.userManager.disableAccount(userDataType);
			}
		}
		
		RequestDispatcher dispatcher = request.getRequestDispatcher("/user/accountManagement");
		dispatcher.forward(request, response);
	}
	private void accountManagementRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		SearchParam param = new SearchParam();
		PagingDataType paging = new PagingDataType();
		int pg=0;
		int pageSize=7;
		
		if(request.getParameter("pg")!= null)
			pg=Integer.parseInt(request.getParameter("pg"));
		
		int startPage = ((pg-1) / pageSize*pageSize)+1;
		int endPage=  ((pg -1) / pageSize*pageSize)+pageSize;
		

		int allPage = this.userManager.selectAllUserCounts();
		
		if(allPage % pageSize == 0)
			allPage = allPage / pageSize;
		else
			allPage = (allPage / pageSize) + 1;
		
		param.setPage(pg*pageSize);
		param.setPageSize(pageSize);
		if(endPage > allPage){
			endPage = allPage;
		}
		List<UserDataType> userList = this.userManager.selectAccounts(param);
		
		paging.setPg(pg);
		paging.setStartPage(startPage);
		paging.setEndPage(endPage);
		paging.setAllPage(allPage);
		paging.setPageSize(pageSize);
		request.setAttribute("paging", paging);
		request.setAttribute("userList", userList);
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/select/admin_userList.jsp");
		dispatcher.forward(request, response);
	}
	private void approvalManagementRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		SearchParam param = new SearchParam();
		PagingDataType paging = new PagingDataType();
		int pg=0;
		int pageSize=7;
		
		if(request.getParameter("pg")!= null)
			pg=Integer.parseInt(request.getParameter("pg"));
		
		int startPage = ((pg-1) / pageSize*pageSize)+1;
		int endPage=  ((pg -1) / pageSize*pageSize)+pageSize;
		

		int allPage = this.userManager.selectAllApprovalCounts();
		
		if(allPage % pageSize == 0)
			allPage = allPage / pageSize;
		else
			allPage = (allPage / pageSize) + 1;
		
		param.setPage(pg*pageSize);
		param.setPageSize(pageSize);
		if(endPage > allPage){
			endPage = allPage;
		}
		
		
		List<ApprovalDataType> approvalList = this.userManager.selectApprovals(param);
		paging.setPg(pg);
		paging.setStartPage(startPage);
		paging.setEndPage(endPage);
		paging.setAllPage(allPage);
		paging.setPageSize(pageSize);
		request.setAttribute("paging", paging);
		request.setAttribute("approvalList", approvalList);
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/select/admin_approvalList.jsp");
		dispatcher.forward(request, response);
	}
	private void userManagementRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/select/admin_userManagement.jsp");
		dispatcher.forward(request, response);
	}
	private void disableAccountRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		if(session.getAttribute("user") == null)
		{
			response.sendRedirect("/ADRM/user/login");
		}
		
		UserDataType userDataType = (UserDataType)session.getAttribute("user");
		userDataType.setDisable("Y");
		
		this.userManager.disableAccount(userDataType);
		
		response.sendRedirect("/ADRM/user/login");
		return;
	}
	
	private void modifyMemberEndRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//TODO hidden(의사) 으로 받은 값이 null이 아니면 approval 삭제 안하기, changeInform 파라미터 한개 더 넣기
		
		request.setCharacterEncoding("UTF-8");
		HttpSession session = request.getSession();
		if(session.getAttribute("user") == null)
		{
			response.sendRedirect("/ADRM/user/login");
		}
		
		UserDataType user = (UserDataType)session.getAttribute("user");
		
		ApprovalDataType approval = new ApprovalDataType();
		approval.setUserId(user.getUserId());
		if(!this.userManager.selectApprovalById(approval).isEmpty() ){
			if(this.userManager.selectApprovalById(approval).get(0).getApproval().equals(ApprovalType.REJECTION)
					|| this.userManager.selectApprovalById(approval).get(0).getApproval().equals(ApprovalType.WAITING))
			{
				this.userManager.deleteApproval(approval);
			}
			
		}
				
		boolean isDoctor = false;
		List<UserRoleDataType> roleList = this.userManager.selectRoles(user);
		if(request.getParameter("isDoctor") != null){
			for(UserRoleDataType role : roleList){
				if(role.getUserRole().equals(UserRoleType.DOCTOR)){
					isDoctor = true;
					break;
				}
			}
		}
		else
			isDoctor = false;
		
		UserDataType userDataType = new UserDataType();
		
		userDataType.setName(request.getParameter("name"));
		String password = Utils.encryptSHA256(request.getParameter("password"));
		userDataType.setPassword(password);
		userDataType.setUserId(user.getUserId());
		userDataType.setGender(request.getParameter("gender"));
		userDataType.setCbnuCode(request.getParameter("cbnuCode"));
		userDataType.setDisable("N");
		List<UserRoleDataType> roles = Utils.setRoles(userDataType.getUserId(), request.getParameterValues("roles"));
					
		userDataType.setRoles(roles);
		
		this.userManager.changeUserInform(userDataType, isDoctor);
		
		response.sendRedirect("/ADRM/user/login");
		return;
	}

	private void modifyMemberRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		if(session.getAttribute("user") == null)
		{
			response.sendRedirect("/ADRM/user/login");
		}
		UserDataType userDataType = (UserDataType) session.getAttribute("user");
		request.setAttribute("user", userDataType);
		List<UserRoleType> userRoleList = Utils.getRoles(this.userManager.selectRoles(userDataType));
		request.setAttribute("userRoles", userRoleList);
		
		for(UserRoleType role : userRoleList){
			if(role.equals(UserRoleType.DOCTOR)){
				request.setAttribute("isDoctor", true);
			}
		}
		
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/login/modifyMember.jsp");
		dispatcher.forward(request, response);
	}
	private void cbnuCodeCheckRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		UserDataType userDataType = new UserDataType();
		userDataType.setCbnuCode(request.getParameter("cbnuCode"));
		JSONObject json = new JSONObject();
		String msg = userDataType.getCbnuCode();

		
		if(this.userManager.cbnuCodeCheck(userDataType) == 0){

			json.put("isDupl", false);
		}
		else{

			json.put("isDupl", true);
		}
		json.put("msg",msg);

		PrintWriter out = response.getWriter();
		out.print(json);

	}
	
	private void idCheckRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		
		UserDataType userDataType = new UserDataType();
		userDataType.setUserId(request.getParameter("userId"));
		JSONObject json = new JSONObject();
		String msg=userDataType.getUserId();
		
		if(this.userManager.idCheck(userDataType) == 0){

			json.put("isDupl", false);
		}
		else{

			json.put("isDupl", true);
		}
		json.put("msg",msg);

		PrintWriter out = response.getWriter();
		out.print(json);

	}

	private void logoutRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		session.setAttribute("user","");
		
		RequestDispatcher dispatcher = request.getRequestDispatcher("/index.jsp");
		dispatcher.forward(request, response);
		
	}
	
	private void mainRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		ApprovalDataType approvalDataType = new ApprovalDataType();
		ApprovalType approvalType = ApprovalType.valueOf("REJECTION");
		approvalDataType.setApproval(approvalType);
		if(session.getAttribute("user") == null)
		{
			response.sendRedirect("/ADRM/user/login");
		}
		UserDataType userDataType = (UserDataType) session.getAttribute("user");
		approvalDataType.setUserId(userDataType.getUserId());
		if(this.userManager.selectApprovalById(approvalDataType).isEmpty()){
		}
		else{
			if(this.userManager.selectApprovalById(approvalDataType).get(0).getApproval().equals(ApprovalType.REJECTION)){
				request.setAttribute("notice", true);
			}
		}

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/select/main.jsp");
		dispatcher.forward(request, response);
	}
	private void loginCheckRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		
		UserDataType userDataType = new UserDataType();
		userDataType.setUserId(request.getParameter("userId"));
		String password = Utils.encryptSHA256(request.getParameter("password"));
		userDataType.setPassword(password);
		
		
		
		UserDataType result = this.userManager.checkLogin(userDataType);

		String resultParameter = null;
		if(result == null)
		{
			resultParameter = "/WEB-INF/jsp/login/login.jsp?wrongValue=true";
		}
		else if(result.getDisable().equals("Y"))
		{
			resultParameter = "/WEB-INF/jsp/login/login.jsp?disable=true";
		}
		else
		{
			HttpSession session = request.getSession();
			
			result.setRoles(this.userManager.selectRoles(result));
			result.setUserId(request.getParameter("userId"));
			
			session.setAttribute("user", result);
			
			resultParameter = "/user/main";
		}
		RequestDispatcher dispatcher = request.getRequestDispatcher(resultParameter);
		dispatcher.forward(request, response);
	}
	private void membershipRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/login/membership.jsp");
		dispatcher.forward(request, response);
	}
	private void loginRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/login/login.jsp");
		dispatcher.forward(request, response);
	}
	private void membershipEndRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		UserDataType userDataType = new UserDataType();
		
		userDataType.setName(request.getParameter("name"));
		
		String password = Utils.encryptSHA256(request.getParameter("password"));
		userDataType.setPassword(password);
		userDataType.setUserId(request.getParameter("userId"));
		userDataType.setGender(request.getParameter("gender"));
		if(request.getParameter("cbnuCode") == "")
			userDataType.setCbnuCode(null);
		else
			userDataType.setCbnuCode(request.getParameter("cbnuCode"));
		userDataType.setDisable("N");
		
		List<UserRoleDataType> roles = Utils.setRoles(userDataType.getUserId(), request.getParameterValues("roles"));
		
		
		userDataType.setRoles(roles);
		
		this.userManager.insertUser(userDataType);
		
		response.sendRedirect("/ADRM/user/login");
		return;
	}

}
