package kr.ac.cbnu.computerengineering.user.manager.dao;

import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;

import kr.ac.cbnu.computerengineering.common.datatype.UserDataType;
import kr.ac.cbnu.computerengineering.common.datatype.UserRoleDataType;
import kr.ac.cbnu.computerengineering.common.managers.dao.IRoleDao;
import kr.ac.cbnu.computerengineering.common.mybatis.Mybatis;

public class RoleDaoImpl implements IRoleDao {

	private SqlSessionFactory sqlSessionFactory;
	
	public RoleDaoImpl(){
		this.sqlSessionFactory = Mybatis.getSqlSessionFactory();
	}

	public void insert(UserRoleDataType userRole){
		SqlSession session = this.sqlSessionFactory.openSession(true);
		try {
			session.insert("user.insertRoles", userRole);
		} catch(Exception e) {
			e.printStackTrace();
		} finally{
			session.close();
		}
	}
	public List<UserRoleDataType> detailsRoles(UserDataType userDataType){
		SqlSession session = this.sqlSessionFactory.openSession();
		List<UserRoleDataType> result=null;
		try{
			result = session.selectList("user.detailUserRole", userDataType);
		} finally{
			session.close();
		}
		return result;
	}
	public void deleteUserRoleInform(UserDataType userDataType){
		SqlSession session = this.sqlSessionFactory.openSession(true);
		try {
			session.delete("user.deleteUserRoleInform", userDataType);
		} finally {
			session.close();
		}
		
	}
	public void insertApproval(UserRoleDataType userRole){
		SqlSession session = this.sqlSessionFactory.openSession(true);
		try {
			session.insert("user.insertApproval", userRole);
		} catch(Exception e) {
			e.printStackTrace();
		} finally{
			session.close();
		}
	}
	public void insertDoctorRole(UserRoleDataType userRoleDataType){
		SqlSession session = this.sqlSessionFactory.openSession(true);
		try {
			session.insert("user.insertRoles", userRoleDataType);
		} catch(Exception e) {
			e.printStackTrace();
		} finally{
			session.close();
		}
	}
	public void deleteRole(UserRoleDataType userRoleDataType){
		SqlSession session = this.sqlSessionFactory.openSession(true);
		try {
			session.delete("user.deleteRole", userRoleDataType);
		} catch(Exception e) {
			e.printStackTrace();
		} finally{
			session.close();
		}
	}
	public List<UserRoleDataType> selectRole(UserRoleDataType userRoleDataType){
		SqlSession session = this.sqlSessionFactory.openSession();
		List<UserRoleDataType> result = null;
		try {
			result = session.selectList("user.selectRole", userRoleDataType);
		} catch(Exception e) {
			e.printStackTrace();
		} finally{
			session.close();
		}
		return result;
	}
}
