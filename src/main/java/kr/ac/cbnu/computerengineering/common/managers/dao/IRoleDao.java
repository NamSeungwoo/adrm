package kr.ac.cbnu.computerengineering.common.managers.dao;

import java.util.List;

import kr.ac.cbnu.computerengineering.common.datatype.UserDataType;
import kr.ac.cbnu.computerengineering.common.datatype.UserRoleDataType;

public interface IRoleDao {
	public void insert(UserRoleDataType userRole);
	public List<UserRoleDataType> detailsRoles(UserDataType userDataType);
	public void deleteUserRoleInform(UserDataType userDataType);
	public void insertApproval(UserRoleDataType userRole);
	public void deleteRole(UserRoleDataType userRole);
	public List<UserRoleDataType> selectRole(UserRoleDataType userRoleDataType);
}

