package kr.ac.cbnu.computerengineering.common.datatype;

import java.util.Date;

public class ProhibitionDataType {

	private int registrationIdx;
	private MedicineDataType prohibitedMedicine;
	private Date prohibitionDate;
	private int idx;
	
	public int getIdx() {
		return idx;
	}
	public void setIdx(int idx) {
		this.idx = idx;
	}
	
	public Date getProhibitionDate() {
		return prohibitionDate;
	}
	public void setProhibitionDate(Date prohibitionDate) {
		this.prohibitionDate = prohibitionDate;
	}
	public int getRegistrationIdx() {
		return registrationIdx;
	}
	public void setRegistrationIdx(int registrationIdx) {
		this.registrationIdx = registrationIdx;
	}
	public MedicineDataType getProhibitedMedicine() {
		return prohibitedMedicine;
	}
	public void setProhibitedMedicine(MedicineDataType prohibitedMedicine) {
		this.prohibitedMedicine = prohibitedMedicine;
	}
	
}
