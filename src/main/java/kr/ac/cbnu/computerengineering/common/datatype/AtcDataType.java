package kr.ac.cbnu.computerengineering.common.datatype;

public class AtcDataType {
	public String atcCode;
	public String atcName;
	
	public String getAtcCode() {
		return atcCode;
	}
	public void setAtcCode(String atcCode) {
		this.atcCode = atcCode;
	}
	public String getAtcName() {
		return atcName;
	}
	public void setAtcName(String atcName) {
		this.atcName = atcName;
	}
	
}
