package kr.ac.cbnu.computerengineering.common.managers;

import java.util.List;

import kr.ac.cbnu.computerengineering.common.datatype.MedicineDataType;
import kr.ac.cbnu.computerengineering.common.datatype.SearchParam;

public interface IMedicineManager {
	public List<MedicineDataType> getMedicineList(SearchParam param);
	public int uploadExcel(MedicineDataType param);
	public int uploadExcel_error(MedicineDataType param);
	public int uploadExcelAtc(MedicineDataType param);
	public int deleteExcel();
	public int deleteExcelAtc();
	public int checkDataBase();
	public int checkAtc();
	public int deleteDataBaseBlank();
	public int deleteAtcBlank();
	public List<MedicineDataType> selectATCByMedicineName(SearchParam param);
	
}
