package kr.ac.cbnu.computerengineering.common.datatype;

import java.util.Date;

public class CapableDataType {
	
	
	private int registrationIdx;
	private MedicineDataType capableMedicine;
	private Date capableDate;
	private int idx;
	
	public int getIdx() {
		return idx;
	}
	public void setIdx(int idx) {
		this.idx = idx;
	}
	public Date getCapableDate() {
		return capableDate;
	}
	public void setCapableDate(Date capableDate) {
		
		this.capableDate = capableDate;
	}
	public int getRegistrationIdx() {
		return registrationIdx;
	}
	public void setRegistrationIdx(int registrationIdx) {
		this.registrationIdx = registrationIdx;
	}
	public MedicineDataType getCapableMedicine() {
		return capableMedicine;
	}
	public void setCapableMedicine(MedicineDataType capableMedicine) {
		this.capableMedicine = capableMedicine;
	}
	
}
