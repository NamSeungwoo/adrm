package kr.ac.cbnu.computerengineering.common.managers;

import java.util.List;

import kr.ac.cbnu.computerengineering.common.datatype.AtcDataType;
import kr.ac.cbnu.computerengineering.common.datatype.CapableDataType;
import kr.ac.cbnu.computerengineering.common.datatype.MedicineDataType;
import kr.ac.cbnu.computerengineering.common.datatype.ProhibitionDataType;
import kr.ac.cbnu.computerengineering.common.datatype.RegistrationDataType;
import kr.ac.cbnu.computerengineering.common.datatype.SearchParam;
import kr.ac.cbnu.computerengineering.common.datatype.UpperDataType;
import kr.ac.cbnu.computerengineering.common.datatype.UserDataType;

public interface IPrescriptionManager {

	public void insertProhibition(ProhibitionDataType prescriptionDataType);
	public void insertUpperAtc(UpperDataType upperDataType);
	public void insertCapable(CapableDataType capableDataType);
	public List<MedicineDataType> alergyBan(SearchParam param);
	public RegistrationDataType selectRegiByParam(SearchParam param);
	public MedicineDataType selectMedicineByCode(SearchParam param);
	public MedicineDataType medicineCheck(SearchParam param);
	public AtcDataType atcCheck(SearchParam param);
	public ProhibitionDataType selectProhibitionByCode(SearchParam param);
	public ProhibitionDataType selectProhibitionMedicineByParams(SearchParam param);
	public int confirmProhibitionByIdx(SearchParam param);
	public int confirmUpperAtcByIdx(SearchParam param);
	public int confirmCapableByIdx(SearchParam param);
	public void deleteProhibition(SearchParam param);
	public void deleteUpperAtc(SearchParam param);
	public void deleteCapable(SearchParam param);
	public List<ProhibitionDataType> selectProhibitionByIdx(SearchParam param);
	public List<UpperDataType> selectUpperAtcByIdx(SearchParam param);
	public List<CapableDataType> selectCapableByIdx(SearchParam param);
	public List<RegistrationDataType> selectRegiDoctorByPatientId(UserDataType userDataType);
}
