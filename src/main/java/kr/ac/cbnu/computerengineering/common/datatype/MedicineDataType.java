package kr.ac.cbnu.computerengineering.common.datatype;

public class MedicineDataType {
	
	private String code;
	private String fullName;
	private String name;
	private AtcDataType atc;
	
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getFullName() {
		return fullName;
	}
	public void setFullName(String fullName) {
		this.fullName = fullName;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public AtcDataType getAtc() {
		return atc;
	}
	public void setAtc(AtcDataType atc) {
		this.atc = atc;
	}
	
	
	
}
