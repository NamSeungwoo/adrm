package kr.ac.cbnu.computerengineering.common.util;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.apache.tomcat.util.codec.binary.Base64;

import kr.ac.cbnu.computerengineering.common.datatype.UserDataType;
import kr.ac.cbnu.computerengineering.common.datatype.UserRoleDataType;
import kr.ac.cbnu.computerengineering.common.datatype.UserRoleType;

public class Utils {
	
	public static enum errorCheck{PROHIBITION_SAME_CAPABLE, CAPABLE_SAME_UPPERATC, PROHIBITION_SAME_UPPERATC, UPPERATC_NOT_IN_PROHIBITION, UPPERATC_NOT_IN_CAPABLE}
	
	public static int roleCnt(UserDataType userDataType)
	{
		int result = 0;
		result = userDataType.getRoles().size();
		return result;
	}
	public static HttpSession setSessionRoles(HttpSession session, List<UserRoleType> list){
		// 
		int i=1;
		for(UserRoleType type: list){
			session.setAttribute("role"+i,type.toString());
			i++;
		}
		return session;
	}
	
	public static List<UserRoleType> getRoles(List<UserRoleDataType> list){
		List<UserRoleType> result = new ArrayList<>();
		
		for(int i=0; i<list.size(); i++){
			result.add(list.get(i).getUserRole());
		}
		return result;
		
	}
	public static List<UserRoleDataType> setRoles(String userId, String[] str){
		List<UserRoleDataType> result = new ArrayList<>();
		
		for(int i=0; i<str.length; i++){
			UserRoleDataType dataType = new UserRoleDataType();
			dataType.setUserId(userId);
			switch(Integer.parseInt(str[i])) {
				case 1:
					dataType.setUserRole(UserRoleType.ADMIN);
					break;
				case 2:
					dataType.setUserRole(UserRoleType.DOCTOR);
					break;
				case 3:
					dataType.setUserRole(UserRoleType.PATIENT);
					break;
				default:
					break;
					
			}
			result.add(dataType);
		}
			
		return result;
		
	}
	public static String encryptSHA256(String str){
		String sha = "";
		try{
			MessageDigest sh = MessageDigest.getInstance("SHA-256");
			sh.update(str.getBytes());
			byte byteData[] = sh.digest();
			StringBuffer sb = new StringBuffer();
			for(int i=0; i<byteData.length; i++){
				sb.append(Integer.toString((byteData[i]&0xff) + 0x100, 16).substring(1));
			}
			sha = sb.toString();
		}catch(NoSuchAlgorithmException e){
			sha = null;
		}
		return sha;
	}
	public static String base64 (String str){
		
		byte[] encoded = Base64.encodeBase64(str.getBytes());
		
		return encoded.toString();
	}
	
}
