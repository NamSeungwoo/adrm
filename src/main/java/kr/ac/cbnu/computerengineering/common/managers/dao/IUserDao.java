package kr.ac.cbnu.computerengineering.common.managers.dao;

import java.util.List;

import kr.ac.cbnu.computerengineering.common.datatype.ApprovalDataType;
import kr.ac.cbnu.computerengineering.common.datatype.SearchParam;
import kr.ac.cbnu.computerengineering.common.datatype.UserDataType;
import kr.ac.cbnu.computerengineering.common.datatype.UserRoleDataType;

public interface IUserDao {
	public UserDataType detailUser(UserDataType userDataType);
	public int insert(UserDataType userDataType);
	public int idCheck(UserDataType userDataType);
	public void updateUserInform(UserDataType userDataType);
	public void disableAccount(UserDataType userDataType);
	public List<UserDataType> selectAccounts(SearchParam param);
	public int selectAllUserCounts();
	public void deleteUser(UserDataType userDataType);
	public void deleteUserRoleInform(UserDataType userDataType);
	public List<UserDataType> selectAccountById(SearchParam param);
	public List<UserDataType> selectAccountByName(SearchParam param);
	public List<ApprovalDataType> selectApprovals(SearchParam param);
	public int selectAllApprovalCounts();
	public List<ApprovalDataType> selectApprovalById(ApprovalDataType approvalDataType);
	public void updateApprovalById(ApprovalDataType approvalDataType);
	public void deleteApproval(ApprovalDataType approvalDataType);
	public int cbnuCodeCheck(UserDataType userDataType);
	public void insertRoleAndUpdateApproval(UserRoleDataType userRoleDataType, ApprovalDataType approvalDataType);
	public void deleteRoleAndUpdateApproval(UserRoleDataType userRoleDataType, ApprovalDataType approvalDataType);
	public void deleteApprovalAndInsertRole(UserRoleDataType userRoleDataType, ApprovalDataType approvalDataType);
	public UserDataType selectIdByCbnuCode(UserDataType userDataType);
	public UserDataType selectMailByIdAndCode(UserDataType userDataType);
	public void changePassword(UserDataType userDataType);
}
