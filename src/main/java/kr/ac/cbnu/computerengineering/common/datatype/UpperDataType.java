package kr.ac.cbnu.computerengineering.common.datatype;

import java.util.Date;

public class UpperDataType {
	
	private int registrationIdx;
	private AtcDataType atc;
	private Date upperDate;
	private int idx;
	
	public int getIdx() {
		return idx;
	}
	public void setIdx(int idx) {
		this.idx = idx;
	}
	
	public Date getUpperDate() {
		return upperDate;
	}
	public void setUpperDate(Date upperDate) {
		this.upperDate = upperDate;
	}
	public int getRegistrationIdx() {
		return registrationIdx;
	}
	public void setRegistrationIdx(int registrationIdx) {
		this.registrationIdx = registrationIdx;
	}
	public AtcDataType getAtc() {
		return atc;
	}
	public void setAtc(AtcDataType atc) {
		this.atc = atc;
	}
}
