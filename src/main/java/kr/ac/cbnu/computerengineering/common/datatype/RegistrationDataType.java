package kr.ac.cbnu.computerengineering.common.datatype;

import java.util.Date;

public class RegistrationDataType {
	private int idx;
	private UserDataType doctor;
	private UserDataType patient;
	private Date registrationDate;
	
	public Date getRegistrationDate() {
		return registrationDate;
	}
	public void setRegistrationDate(Date registrationDate) {
		this.registrationDate = registrationDate;
	}
	public int getIdx() {
		return idx;
	}
	public void setIdx(int idx) {
		this.idx = idx;
	}
	public UserDataType getDoctor() {
		return doctor;
	}
	public void setDoctor(UserDataType doctor) {
		this.doctor = doctor;
	}
	public UserDataType getPatient() {
		return patient;
	}
	public void setPatient(UserDataType patient) {
		this.patient = patient;
	}
	
	
}
