package kr.ac.cbnu.computerengineering.common.datatype;

public class SearchParam {
	private String param;
	private String id;
	private String cbnuCode;
	private int idx;
	private String medicineCode;
	private String atcFirst;
	private int page;
	private int pageSize;
	
	
	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public String getAtcFirst() {
		return atcFirst;
	}

	public void setAtcFirst(String atcFirst) {
		this.atcFirst = atcFirst;
	}

	public String getMedicineCode() {
		return medicineCode;
	}

	public void setMedicineCode(String medicineCode) {
		this.medicineCode = medicineCode;
	}

	public int getIdx() {
		return idx;
	}

	public void setIdx(int idx) {
		this.idx = idx;
	}

	public String getCbnuCode() {
		return cbnuCode;
	}

	public void setCbnuCode(String cbnuCode) {
		this.cbnuCode = cbnuCode;
	}

	public String getParam() {
		return param;
	}
	
	public String getId() {
		return id;
	}

	public void setParam(String param) {
		this.param = param;
	}
	
	public void setId(String id){
		this.id = id;
	}
	
}
